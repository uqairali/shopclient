import React, { Fragment } from 'react'
import { dateFormate, filterCaseInsensitive } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { getAllRecipt, deleteRecipt,AllClientName,allReciptByName } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import { Col, FormGroup, Row, ControlLabel } from 'react-bootstrap'
import { dateFormate2, paramsDateFormet } from 'utils/helper';
import ViewPrintModal from 'components/Modals/viewPrintModal'
import Datetime from "react-datetime";
import Select from "react-select";
class AllRecipt extends React.Component {

    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            showViewModal: false,
            date: new Date(),
            totalsell: 0,
            AllClients:[],
            selectedClient:[]
        }
        this.getProducts()
        this.onGetClientsName()
    }
    onGetReciptsByNames=async(name)=>{
        try{
         var res=await allReciptByName(name)
         var total = 0
         res.data.map(data => {
             total += data.totalAmmount
         })
         this.setState({totalsell:total, Data:res.data})
        }catch(err){
            console.log(err)
        }
    }
  onGetClientsName=async()=>{
        var data=await AllClientName()
         var newData=data.data.map(client=>{
             return{label:client.name,value:client.name}
         })
        this.setState({AllClients:newData})
    }
    getProducts = async () => {
        try {
            const res = await getAllRecipt(paramsDateFormet(this.state.date))
            var total = 0
            res.data.map(data => {
                total += data.totalAmmount
            })
            this.setState({selectedClient:[], totalsell: total, Data: res.data })
        } catch (err) {

        }
    }
    onDeleteProductType = async () => {
        try {
            await deleteRecipt(this.state.selectedObj._id)
            this.getProducts()
        } catch (err) {

        }
    }
    render() {
        const { AllClients,selectedClient,totalsell, date, Data, loading, deleteAlert, showAddProductModal, showEditTypeModal, selectedObj, showViewModal } = this.state
        const data = Data.map((prop, sn) => {

            return {
                sn: sn + 1,
                serialNumber: +prop.serialNumber,
                name: prop.clientName,
                date: dateFormate2(prop.createdAt),
                total: prop.totalAmmount,
                actions: <div className="table-action-btn-wrapper">
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showViewModal: true }))
                    }} title="View and Print" bsStyle="primary"
                        bsSize="xs" fill >
                        < i className={"fa fa-eye"} />
                        <span>
                            < i className={"fa fa-print"} />
                        </span>
                    </Button>
                    <Button onClick={() => {
                        window.open(`/account/ClientReceipt/${prop.clientName}/${prop._id}`)
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ deleteAlert: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />

                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: 'S.N',
                accessor: 'serialNumber',
                filterable: true,
                id: 'serialNumber'
            },
            {
                Header: "name",
                accessor: "name",
                filterable: true,
                id: "name"
            },
            {
                Header: "date",
                accessor: "date",
                filterable: false,
                id: "date"
            },
            {
                Header: "total",
                accessor: "total",
                filterable:true,
                id: "total"
            },
            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row>
                    <Col md={3}>
                        <FormGroup>
                            <ControlLabel>Date</ControlLabel>
                            <Datetime
                                timeFormat={false}
                                inputProps={{ placeholder: "Select date" }}
                                defaultValue={new Date()}
                                onChange={(d) => this.setState({ date: d })}
                                value={date}
                                onBlur={() => this.getProducts({})}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <ControlLabel>Search By Name</ControlLabel>
                            <Select
                                clearable={false}
                                placeholder={"Single Select"}
                                value={selectedClient}
                                options={AllClients}
                                onChange={data => {
                                    if (data) {
                                        this.setState({selectedClient:data})
                                        this.onGetReciptsByNames(data.label)
                                    }
                                }}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <ControlLabel>Total Sell</ControlLabel>
                            <h4 style={{ fontWeight: "bold", color: 'red' }}>{totalsell}</h4>
                        </FormGroup>
                    </Col>


                </Row>
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />

                {
                    deleteAlert &&
                    <DeleteAlert
                        hide={() => this.setState({ deleteAlert: false })}
                        onDelete={this.onDeleteProductType}
                    />
                }
                {showViewModal &&
                    <ViewPrintModal
                        show={showViewModal}
                        hide={() => this.setState({ showViewModal: false })}
                        selectedObj={selectedObj}
                    />
                }

            </div>
        )
    }
}

export default AllRecipt
