import React, { useState, useEffect } from 'react'
import { Grid, Col, Table, Row, FormGroup, ControlLabel } from 'react-bootstrap'
import Button from "components/CustomButton/CustomButton.jsx";
import Card from "components/Card/Card.jsx";
import { connect } from 'react-redux'
import { getTestList, deleteTestList } from 'api/api'
import { dateFormate } from 'utils/helper'
import DeleteAlert from 'components/Modals/deleteAlert'
import AddTestModal from 'components/Modals/addTestModal'
import EditTestModal from 'components/Modals/editTestModal'
import Select from "react-select";
const Test = ({ onShowNotification, onSetLoading, classList }) => {
    const [allTest, setAllTest] = useState([])
    const [selectedObj, setSelectedObj] = useState('')
    const [deleteAlert, setDeleteAlert] = useState(false)
    const [selectedClass, setSelectedClass] = useState('')
    const [onDeleteLoading, setDeleteLoading] = useState(false)
    const [showAddTestModal, setShowTestsModal] = useState(false)
    const [showEditTestModal, setShowEditTestModal] = useState(false)
    useEffect(() => {
        if (selectedClass)
            fetchData()
    }, [selectedClass])
    const onDelete = async () => {
        try {
            setDeleteLoading(true)
            await deleteTestList(selectedObj._id)
            setDeleteLoading(false)
            fetchData()
        } catch (err) {
            setDeleteLoading(false)
            onShowNotification('error', err.message)
        }
    }
    const fetchData = async () => {
        try {
            onSetLoading(true)
            var res = await getTestList(selectedClass._id)
            onSetLoading(false)
            if (res.data) {
                setAllTest(res.data)
            }
        } catch (err) {
            onSetLoading(false)
            onShowNotification('error', err.message)
        }
    }
    return (
        <Grid fluid>
            <Col md={8} mdOffset={2}>
                <Card
                    // title=""

                    //category="Here is a subtitle for this table"
                    tableFullWidth
                    content={
                        <div>
                            <Row>
                                <Col md={3} className="attendance-header-wrapper-btn">
                                    <Button onClick={() => setShowTestsModal(true)} className="btn-submit" fill><i className={"fa fa-plus"} />Add Test</Button>
                                </Col>
                                <Col md={8}>
                                    <FormGroup>
                                        <ControlLabel>Class</ControlLabel>
                                        <Select
                                            clearable={false}
                                            placeholder="Single Select"
                                            value={selectedClass}
                                            options={classList}
                                            onChange={data => {
                                                if (data)
                                                    setSelectedClass(data)
                                            }}
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Table responsive className="custom-table-wrapper" >
                                <thead>
                                    <tr>
                                        <th className="text-center">#</th>
                                        <th>Test NAME</th>
                                        <th>Test Description</th>
                                        <th>Test Date</th>
                                        <th className="text-right">ACTiON</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {allTest.map((itm, key) => (
                                        <tr key={key}>
                                            <td className="text-center">{key + 1}</td>
                                            <td>{itm.name}</td>
                                            <td>{itm.description}</td>
                                            <td>{dateFormate(itm.date)}</td>
                                            <td className="text-right table-action-btn-wrapper">
                                                <Button className="btn-submit" onClick={() => {
                                                    setSelectedObj(itm)
                                                    setShowEditTestModal(true)
                                                }} title="Edit" simple bsStyle="info" bsSize="xs" fill>
                                                    <i className="fa fa-edit" />
                                                </Button>
                                                <Button disabled={onDeleteLoading && selectedObj._id === itm._id} onClick={() => {
                                                    setSelectedObj(itm)
                                                    setDeleteAlert(true)
                                                }} title="Delete" simple bsStyle="danger" bsSize="xs" fill>
                                                    <i className={onDeleteLoading && selectedObj._id === itm._id ? "fa fa-spin fa-spinner" : "fa fa-times"} />
                                                </Button>

                                            </td>
                                        </tr>
                                    ))}


                                </tbody>
                            </Table>
                        </div>
                    }
                />
            </Col>

            {
                deleteAlert &&
                <DeleteAlert
                    hide={() => setDeleteAlert(false)}
                    onDelete={onDelete}
                />
            }
            {
                showAddTestModal &&
                <AddTestModal
                    show={showAddTestModal}
                    hide={() => setShowTestsModal(false)}
                    onSowNotification={onShowNotification}
                    fetchData={setSelectedClass}
                    classList={classList}
                />
            }
            {
                showEditTestModal &&
                <EditTestModal
                    show={showEditTestModal}
                    hide={() => setShowEditTestModal(false)}
                    onSowNotification={onShowNotification}
                    fetchData={setSelectedClass}
                    classList={classList}
                    selectedObj={selectedObj}
                />
            }
        </Grid>
    )
}
const mapStateToProps = state => {
    return {
        classList: state.classList
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
        onSetLoading: (loading) => dispatch({ type: 'SET_LOADING', setLoading: loading })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Test)