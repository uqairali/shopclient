
import React, { Component, Fragment } from "react";
import { Grid, Col, Row } from "react-bootstrap";
// react component used to create charts
import ChartistGraph from "react-chartist";
// react components used to create a SVG / Vector map
import { VectorMap } from "react-jvectormap";
import CountUp from 'react-countup';
import Card from "components/Card/Card.jsx";
import StatsCard from "components/Card/StatsCard.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import { getAllRecipt, getAllAmmount, sillsMenDetails, getAllRecipts } from 'api/api'
import { paramsDateFormet } from '../utils/helper'
import {
  dataPie,
  dataSales,
  optionsSales,
  responsiveSales,
  dataBar,
  optionsBar,
  responsiveBar,
  table_data
} from "variables/Variables.jsx";

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};

class Dashboard extends Component {
  state = {
    AllRecipt: [],
    TodaySell: 0,
    sellsMenAmmount: 0,
    todayPayedAmmount: 0,
    allSales: 0
  }
  componentWillMount() {
    this.onGetAllRecipt(paramsDateFormet(new Date))
    this.onGetAllAmmount()
    this.onGetTodayPayedAmmount()
    this.onGetAllRecipts()
  }
  onGetTodayPayedAmmount = async () => {
    try {
      var data = await sillsMenDetails(paramsDateFormet(new Date()))
      if (data.data) {
        var add = 0;
        data.data.map(itm => {
          if (itm.isPayed) {
            add += itm.payment
          }
        })
        this.setState({ todayPayedAmmount: add })
      }
    } catch (err) {
      console.log(err)
    }
  }
  onGetAllAmmount = async () => {
    try {
      var data = await getAllAmmount()
      if (data.data) {
        var add = 0;
        data.data.map(itm => {
          add += itm.ammount
        })
        this.setState({ sellsMenAmmount: add })
      }
    } catch (err) {
      console.log(err)
    }
  }
  onGetAllRecipt = async (date) => {
    try {
      var Recipt = await getAllRecipt(date)
      var totalCell = 0
      Recipt.data.map(data => {
        totalCell += data.totalAmmount
      })
      this.setState({ TodaySell: totalCell, AllRecipt: Recipt.data })
    }
    catch (err) {
      console.log(err)
    }
  }
  onGetAllRecipts = async () => {
    try {
      var result = await getAllRecipts()
      var totalCell = 0
      result.data.map(data => {
        totalCell += data.totalAmmount
      })
      this.setState({ allSales: totalCell })
    }
    catch (err) {
      console.log(err)
    }
  }
  createTableData() {
    var tableRows = [];
    for (var i = 0; i < table_data.length; i++) {
      tableRows.push(
        <tr key={i}>
          <td>
            <div className="flag">
              <img src={table_data[i].flag} alt="us_flag" />
            </div>
          </td>
          <td>{table_data[i].country}</td>
          <td className="text-right">{table_data[i].count}</td>
          <td className="text-right">{table_data[i].percentage}</td>
        </tr>
      );
    }
    return tableRows;
  }
  render() {
    const { todayPayedAmmount, TodaySell, AllRecipt, sellsMenAmmount, allSales } = this.state
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-user text-warning" />}
                statsText="Today sell"
                statsValue={<Fragment><CountUp end={TodaySell} duration={1} /></Fragment>}
                statsIcon={<i className="pe-7s-play" />}
                statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-user text-success" />}
                statsText="Today Recipts"
                statsValue={<Fragment><CountUp end={AllRecipt.length} duration={5} /></Fragment>}
                statsIcon={<i className="fa fa-calendar-o" />}
                statsIconText="Last day"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-graph1 text-danger" />}
                statsText="Pay All Ammount To Sells Mens"
                statsValue={<Fragment><CountUp end={sellsMenAmmount} duration={1} /></Fragment>}
                statsIcon={<i className="fa fa-clock-o" />}
                statsIconText="In the last hour"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-twitter text-info" />}
                statsText="Today Payed Ammount"
                statsValue={<Fragment>+<CountUp end={todayPayedAmmount} duration={1} /></Fragment>}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
          </Row>
          <Row>
            <Col lg={4} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-twitter text-info" />}
                statsText="Today Payed Ammount"
                statsValue={<Fragment><CountUp end={allSales} duration={1} /></Fragment>}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Card
                title="Global Sales by Top Locations"
                category="All products that were shipped"
                content={
                  <Row>
                    <Col md={5}>
                      <div className="table-responsive">
                        <table className="table">
                          <tbody>{this.createTableData()}</tbody>
                        </table>
                      </div>
                    </Col>
                    <Col md={6} mdOffset={1}>
                      <VectorMap
                        map={"world_mill"}
                        backgroundColor="transparent"
                        zoomOnScroll={false}
                        containerStyle={{
                          width: "100%",
                          height: "280px"
                        }}
                        containerClassName="map"
                        regionStyle={{
                          initial: {
                            fill: "#e4e4e4",
                            "fill-opacity": 0.9,
                            stroke: "none",
                            "stroke-width": 0,
                            "stroke-opacity": 0
                          }
                        }}
                        series={{
                          regions: [
                            {
                              values: mapData,
                              scale: ["#AAAAAA", "#444444"],
                              normalizeFunction: "polynomial"
                            }
                          ]
                        }}
                      />
                    </Col>
                  </Row>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Card
                title="Email Statistics"
                category="Last Campaign Performance"
                content={<ChartistGraph data={dataPie} type="Pie" />}
                legend={
                  <div>
                    <i className="fa fa-circle text-info" /> Open
                    <i className="fa fa-circle text-danger" /> Bounce
                    <i className="fa fa-circle text-warning" /> Unsubscribe
                  </div>
                }
                stats={
                  <div>
                    <i className="fa fa-clock-o" /> Campaign sent 2 days ago
                  </div>
                }
              />
            </Col>
            <Col md={8}>
              <Card
                title="Users Behavior"
                category="24 Hours performance"
                content={
                  <ChartistGraph
                    data={dataSales}
                    type="Line"
                    options={optionsSales}
                    responsiveOptions={responsiveSales}
                  />
                }
                legend={
                  <div>
                    <i className="fa fa-circle text-info" /> Open
                    <i className="fa fa-circle text-danger" /> Click
                    <i className="fa fa-circle text-warning" /> Click Second
                    Time
                  </div>
                }
                stats={
                  <div>
                    <i className="fa fa-history" /> Updated 3 minutes ago
                  </div>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Card
                title="2014 Sales"
                category="All products including Taxes"
                content={
                  <ChartistGraph
                    data={dataBar}
                    type="Bar"
                    options={optionsBar}
                    responsiveOptions={responsiveBar}
                  />
                }
                legend={
                  <div>
                    <i className="fa fa-circle text-info" /> Tesla Model S
                    <i className="fa fa-circle text-danger" /> BMW 5 Series
                  </div>
                }
                stats={
                  <div>
                    <i className="fa fa-check" /> Data information certified
                  </div>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                title="Tasks"
                category="Backend development"
                content={
                  <table className="table">
                    <Tasks />
                  </table>
                }
                stats={
                  <div>
                    <i className="fa fa-history" /> Updated 3 minutes ago
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
