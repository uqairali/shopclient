import React, { Fragment } from 'react'
import { filterCaseInsensitive } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { deleteSellsMen, getAllSellsMen } from 'api/api'
import LoginAlert from '../../components/Modals/loginAlert'
import { Col, FormGroup, Row, ControlLabel } from 'react-bootstrap'
import AddSellsMenModal from 'components/Modals/AddSellsMen'
import EditSellsMenModal from 'components/Modals/EditSellsMen'
import { connect } from 'react-redux'
import AddPayment from 'components/Modals/AddPayment'
import AddBill from 'components/Modals/AddBill'
class ClientsDetails extends React.Component {

    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            showEditModal: false,
            showAddSellsMenModal: false,
            addPaymentModal: false,
            addBillModal: false,
            isSelectToPay: false
        }
        this.getDetails()
    }

    getDetails = async () => {
        try {
            const res = await getAllSellsMen()
            this.setState({ Data: res.data })
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    onDeleteClientDetails = async () => {
        try {
            await deleteSellsMen(this.state.selectedObj._id)
            this.getDetails()
        } catch (err) {

        }
    }
    render() {
        const { isSelectToPay, addBillModal, addPaymentModal, showAddSellsMenModal, Data, loading, deleteAlert, selectedObj, showEditModal } = this.state
        const { onShowNotification } = this.props
        const data = Data.map((prop, sn) => {

            return {
                sn: sn + 1,
                name: prop.name ? prop.name : '-',
                distribution: prop.item ? prop.item : '-',
                contactNumber: prop.contactNumber ? prop.contactNumber : '-',
                ammount: prop.ammount ? prop.ammount : 0,
                actions: <div className="table-action-btn-wrapper">
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop, isSelectToPay: true }, () => this.setState({ addBillModal: true }))
                    }} title="add Bill" bsStyle="secondary"
                        bsSize="xs" fill >
                        < i className={"fa fa-server"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop, isSelectToPay: true }, () => this.setState({ addPaymentModal: true }))
                    }} title="Add Payment" bsStyle="info"
                        bsSize="xs" fill >
                        < i className={"fa fa-gift"} />
                    </Button>
                    <Button onClick={() => {
                        window.open(`/admin/sellsMenDetails/${prop._id}`)
                    }} title="Edit" bsStyle="primary"
                        bsSize="xs" fill >
                        < i className={"fa fa-eye"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showEditModal: true }))
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ deleteAlert: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />

                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: 'name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: "distribution",
                accessor: "distribution",
                filterable: true,
            },
            {
                Header: "contact number",
                accessor: "contactNumber",
                filterable: false,
            },
            {
                Header: "ammount",
                accessor: "ammount",
                filterable: false,
            },

            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row style={{ textAlign: "right" }}>
                    <Col md={4}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ showAddSellsMenModal: true })}>
                            <i className={"fa fa-plus"} />Add New Distribution</Button>
                    </Col>
                    <Col md={4}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addPaymentModal: true })}>
                            <i className={"fa fa-plus"} />Add Payment</Button>
                    </Col>
                    <Col md={4}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addBillModal: true })}>
                            <i className={"fa fa-plus"} />Add Bill</Button>
                    </Col>
                </Row>
                <div className="client-details">
                    <ReactTable
                        title={"test"}
                        data={data}
                        filterable
                        columns={Columns}
                        defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                        // manual
                        defaultPageSize={10}
                        // onFetchData={onFetch}
                        showPaginationBottom
                        showPaginationTop={false}
                        // pages={this.state.pages}
                        loading={loading}
                        sortable={false}
                        className="-striped -highlight"
                    />
                </div>

                {
                    deleteAlert &&
                    <LoginAlert
                        hide={() => this.setState({ deleteAlert: false })}
                        show={deleteAlert}
                        onDelete={()=>this.setState({deleteAlert:false},()=>this.onDeleteClientDetails())}
                    />
                }
                {
                    showAddSellsMenModal &&
                    <AddSellsMenModal
                        show={showAddSellsMenModal}
                        hide={() => this.setState({ showAddSellsMenModal: false })}
                        fetchData={this.getDetails}
                        onSowNotification={onShowNotification}
                    />
                }
                {showEditModal &&
                    <EditSellsMenModal
                        show={showEditModal}
                        hide={() => this.setState({ showEditModal: false })}
                        fetchData={this.getDetails}
                        selectedObj={selectedObj}
                        onSowNotification={onShowNotification}
                    />
                }
                {addPaymentModal &&
                    <AddPayment
                        show={addPaymentModal}
                        hide={() => this.setState({ addPaymentModal: false, isSelectToPay: false })}
                        fetchData={this.getDetails}
                        allData={Data}
                        onSowNotification={onShowNotification}
                        isSelectToPay={isSelectToPay}
                        selectedObj={selectedObj}
                    />
                }
                {addBillModal &&
                    <AddBill
                        show={addBillModal}
                        hide={() => this.setState({ addBillModal: false, isSelectToPay: false })}
                        fetchData={this.getDetails}
                        allData={Data}
                        onSowNotification={onShowNotification}
                        isSelectToPay={isSelectToPay}
                        selectedObj={selectedObj}
                    />
                }


            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
    }
}
export default connect(null, mapDispatchToProps)(ClientsDetails)