import React, { useState, useEffect, Fragment } from 'react'
import Print from 'components/printer'
import Helmet from 'react-helmet'
import { Row, Col, Grid, FormGroup, ControlLabel, Table } from 'react-bootstrap'
import Card from 'components/Card/Card.jsx'
import Select from "react-select";
import { getBarCode, getAllShortKey, updatRecipt, getItems, updateItemCount, postRecipt, getSerialNumber, updateSerialNumber, addClientName, getReciptbyId } from 'api/api'
import { dateFormate2 } from '../../utils/helper'
import Button from "components/CustomButton/CustomButton.jsx";
import Switch from 'react-bootstrap-switch';
import CustomEntry from 'components/customEntry'
import ManagePayment from 'components/managePayment'
import AddReciptModal from 'components/Modals/addReciptModal'
import DeleteAlert from '../../components/Modals/deleteAlert'
import ReadBarCodeModal from 'components/Modals/readBarCode'
import BarcodeReader from 'react-barcode-reader'
import { useSpeechSynthesis, useSpeechRecognition } from 'react-speech-kit';
import { connect  } from "react-redux"
const ClientReceipt = (props) => {
    const [AllItems, setAllItems] = useState([])
    const [selectedItem, setSelectedItem] = useState([])
    const [selectedType, setSelectedType] = useState([])
    const [priceArray, setPriceArray] = useState([])
    const [selectedPrice, setSelectedPrice] = useState([])
    const [count, setCount] = useState(1)
    const [receiptArray, setReceiptArray] = useState([])
    const [deductionArray, setDeductionArray] = useState([])
    const [serialNumber, setSerialNumber] = useState([])
    const [onSubTotal, setSubTotal] = useState(0)
    const [isPendingPrint, setIspendingPrint] = useState(false)
    const [isPendingPayment, setIsPendingPayment] = useState(true)
    const [discount, setDiscount] = useState('')
    const [isUpdateRecipt, setIsUpdateRecipt] = useState(false)
    const [updatedReciptID, setUpdatedReciptID] = useState('')
    const [isAutoSelect, setIsAutoSelect] = useState(true)
    const [isPayment, setIsPayment] = useState(false)
    const [payment, setPayment] = useState('')
    const [remining, setRemining] = useState('')
    const [showAddingReciptModal, setShowReciptModal] = useState(false)
    const [shortKey, setShortKey] = useState('')
    const [allShortKeys, setAllShortKeys] = useState([])
    const [notificationAlert, setNotificationAlert] = useState(false)
    const [recivedAmmount, setRecivedAmmount] = useState('')
    const [readBarCode, setReadBarCode] = useState('')
    const [showReadBarCodeModal, setShowReadBarCodeModal] = useState(false)
    const [barCodeArray, setBarCodeArray] = useState([])
    const [backDeductionArray, setBackDeductionArray] = useState([])
    const [speachValue,setSpeachValue]=useState("")
    const { listen, listening, stop } = useSpeechRecognition({
        onResult: (result) => {
            setSpeachValue(result)
        },
    });
    // useEffect(()=>{
    //  if(speachValue)
    //  setShortKey(speachValue)
    // },[speachValue])
    useEffect(() => {
        if (readBarCode !== "") {
            if (!showReadBarCodeModal) {
                onHandleExestense()
                setShowReadBarCodeModal(true)
            } else {
                onHandleExestense()
            }
        }
    }, [readBarCode])
    useEffect(() => {
        if (isUpdateRecipt && AllItems.length >= 1)
            onAddBackDeduction(deductionArray)
    }, [AllItems])
    const { speak } = useSpeechSynthesis();
    const onHandleExestense = async () => {
        try {
            var data = await getBarCode(readBarCode)
            if (data.data) {
                var newArray = [...barCodeArray]
                var find = false
                newArray.map((dt, key) => {
                    if (dt.typeId === data.data._id) {
                        find = true;
                        newArray[key].count += 1
                        newArray[key].subTotal += data.data.price
                    }
                })
                if (!find) {
                    var newObj = {
                        count: 1,
                        item: data.data.name,
                        particular: '',
                        price: data.data.price,
                        subTotal: data.data.price,
                        type: data.data.name,
                        typeId: data.data._id,
                        isCustomEntry: true
                    }
                    newArray.push(newObj)
                }
                setBarCodeArray(newArray)
                setReadBarCode('')
            } else {
                speak({ text: "scan not found. please register first" })
            }

        } catch (err) {
        }
    }
    useEffect(() => {
        if (shortKey !== "") {
            try {
                var filterData = allShortKeys.filter(dt => +dt.key === +shortKey)
                if (filterData.length) {
                    var itm = filterData[0]

                    const data = {
                        label: itm.itemTypeId.itemId.name, value: itm.itemTypeId.itemId.name,
                        types: [{ label: itm.itemTypeId.name, value: itm.itemTypeId.name, type: itm.itemTypeId }]

                    }
                    var newPriceArray = data.types[0].type.price.map(pr => {
                        return { label: pr.particular, value: pr.particular, price: pr }
                    })
                    setSelectedPrice([])
                    setPriceArray(newPriceArray)
                    setSelectedPrice(newPriceArray[0])
                    setSelectedType({ label: itm.itemTypeId.name, value: itm.itemTypeId.name, type: itm.itemTypeId })
                    setSelectedItem(data)
                } else {
                    setSelectedItem([])
                    setSelectedType([])
                    setSelectedPrice([])
                    setPriceArray([])
                    setCount(1)
                    setDiscount(0)
                }
            }
            catch (err) {
                props.onShowNotification("error",err.message)
            }
        }
    }, [shortKey])
    const handleBarCode = (data) => {
        setReadBarCode(data)
    }
    var subTotal = 0;
    receiptArray.map(val => {
        subTotal += val.subTotal
    })
    useEffect(() => {
        if (props.match.params.reciptId) {
            onGetReciptById(props.match.params.reciptId)
        }
        onGetAllItems()
        onSaveClientName()
        onIncrementSerialNumber()
        onGetAllShortKeys()
    }, [])

    const onGetAllShortKeys = async () => {
        try {
            var data = await getAllShortKey()
            setAllShortKeys(data.data)
        } catch (err) {
            console.log(err)
        }
    }

    const onIncrementSerialNumber = async () => {
        try {
            await updateSerialNumber()
        } catch (err) {
            console.log(err)
        }
    }
    const onGetReciptById = async (id) => {
        try {
            var data = await getReciptbyId(id)
            setReceiptArray(data.data.recipt)

            setUpdatedReciptID(data.data._id)
            setDeductionArray(data.data.deductionArray)
            setIsUpdateRecipt(true)
            if (data.data.pending)
                setRemining(data.data.pending)
            if (data.data.paid)
                setPayment(data.data.paid)
        } catch (err) {
            console.log(err)
        }
    }
    const onAddBackDeduction = (deduction) => {
        var newDeductionArray = []
        deduction.map(ded => {
            try {
                var totalCount = { ...ded.count }
                var findItemType, findRecipt;
                AllItems.map(itm => {
                    itm.types.map(typ => {
                        if (typ.type._id === ded.typeId)
                            findItemType = typ.type
                    })
                })
                receiptArray.map(rec => {
                    if (rec.typeId === ded.typeId)
                        findRecipt = rec
                })
                if (findRecipt.particular === totalCount.particular) {
                    totalCount.totalCount = findItemType.count.totalCount + findRecipt.count
                }
                else {
                    const filterPriceArray = findItemType.price.filter(val => val.particular === totalCount.particular)
                    totalCount.addition = findItemType.count.addition - (+findRecipt.count);
                }
                newDeductionArray.push({ typeId: ded.typeId, count: totalCount })
                setBackDeductionArray(newDeductionArray)
            } catch (err) {
                alert(err)
            }
        })

    }
    const onSaveClientName = async () => {
        var data = {
            name: props.match.params.name
        }
        try {
            await addClientName(data)
        } catch (err) {
            console.error(err)
        }
    }
    const onSuccessPrint = () => {
        setSubTotal(subTotal)
        setReceiptArray([])
        setDeductionArray([])
        onGetAllItems()
        setIspendingPrint(false)
        setTimeout(() => {
            window.close()
        }, 900000);

    }
    const onBeforePrint = async () => {
        const recipt = {
            clientName: props.match.params.name,
            serialNumber: serialNumber.serialNumber,
            totalAmmount: 0,
            recipt: receiptArray,
            deductionArray: deductionArray,
            pending: remining ? remining : 0,
            paid: payment ? payment : 0
        }

        receiptArray.map(val => {
            recipt.totalAmmount += +val.subTotal
        })
        return new Promise(async (resolve, reject) => {
            try {
                if (isUpdateRecipt && backDeductionArray.length)
                    await updateItemCount(backDeductionArray)
                if (deductionArray.length)
                    await updateItemCount(deductionArray)
                if (isUpdateRecipt) {
                    await updatRecipt(updatedReciptID, recipt)
                } else {
                    await postRecipt(recipt)
                }
                setIspendingPrint(true)
                resolve(true)
            } catch (err) {
                console.log(err)
                reject(err)
            }
        }
        )

    }

    const onRemotePrint = async () => {
        try {
            setIspendingPrint(true)
            const recipt = {
                clientName: props.match.params.name,
                serialNumber: serialNumber.serialNumber,
                totalAmmount: 0,
                recipt: receiptArray,
                isPendingPrint: true,
                deductionArray: deductionArray,
                pending: remining ? remining : 0,
                paid: payment ? payment : 0
            }

            receiptArray.map(val => {
                recipt.totalAmmount += +val.subTotal
            })
            if (isUpdateRecipt && backDeductionArray.length)
                await updateItemCount(backDeductionArray)
            if (deductionArray.length)
                await updateItemCount(deductionArray)
            if (isUpdateRecipt) {
                await updatRecipt(updatedReciptID, recipt)
            } else {
                await postRecipt(recipt)
            }
            setSubTotal(subTotal)
            setReceiptArray([])
            setDeductionArray([])
            onGetAllItems()
            setIspendingPrint(false)
            setTimeout(() => {
                window.close()
            }, 900000);
        }
        catch (err) {
            console.log("err", err)
        }

    }
    const onGetAllItems = async () => {
        try {
            const allItems = await getItems()
            const reciptSN = await getSerialNumber()

            setSerialNumber(reciptSN.data[0])
            const data = allItems.data.map(itm => {
                return {
                    label: itm.name, value: itm.name, types: itm.types.map(typ => {
                        return { label: typ.name, value: typ.name, type: typ }
                    })
                }
            })
            setAllItems(data)

        }
        catch (err) {
            console.log(err)
        }
    }
    const onDeduction = () => {

        try {
            var newDeductionArray = [...deductionArray]
            var totalCount = {
                totalCount: selectedType.type.count.totalCount,
                particular: selectedType.type.count.particular,
                addition: selectedType.type.count.addition ? selectedType.type.count.addition : 0
            }
            if (selectedType.type.count.particular === selectedPrice.label) {
                totalCount.totalCount = totalCount.totalCount - count
            }
            else {

                const filterPriceArray = selectedType.type.price.filter(val => val.particular === selectedType.type.count.particular)
                totalCount.addition = totalCount.addition + (+selectedPrice.price.contain * count);
                if (totalCount.addition >= (+filterPriceArray[0].contain)) {
                    totalCount.totalCount = totalCount.totalCount - (Math.floor(totalCount.addition / (+filterPriceArray[0].contain)))
                    totalCount.addition = totalCount.addition - ((Math.floor(totalCount.addition / (+filterPriceArray[0].contain))) * (+filterPriceArray[0].contain))
                }
            }
            newDeductionArray.push({ typeId: selectedType.type._id, count: totalCount })
            setDeductionArray(newDeductionArray)
        } catch (err) {
            alert(err)
        }
    }
    const handleOnClick = () => {
        if (selectedType.type.count.particular === selectedPrice.label && selectedType.type.count.totalCount < count) {
            setNotificationAlert(true)
        } else {
            handleOnAdd()
        }
    }
    const handleOnAdd = () => {
        var newArray = [...receiptArray]
        var newDiscount = discount === "" ? 0 : discount
        newArray.push({
            item: selectedItem.label, type: selectedType.label,
            particular: selectedPrice.label, price: selectedPrice.price.price - newDiscount,
            count: count, subTotal: (selectedPrice.price.price - newDiscount) * count, typeId: selectedType.type._id
        })
        onDeduction()
        setReceiptArray(newArray)
        setSelectedItem([])
        setSelectedType([])
        setSelectedPrice([])
        setPriceArray([])
        setCount(1)
        setDiscount(0)
        setShortKey('')
    }
    const onCalculateSubTotal = () => {
        var Total = receiptArray.length ? subTotal : onSubTotal;

        if (remining) {
            Total += +remining
        }
        if (payment) {
            Total = Total - payment
        }
        return Total
    }


    return (
        <Grid fluid className="client_receipt_wrapper">
            <Helmet>
                <title>{props.match.params.name}</title>
            </Helmet>
            <Row>
                <Col md={8}>
                    <Card
                        title="ITEMS"
                        content={
                            isPayment ? <ManagePayment
                                payment={payment}
                                setPayment={setPayment}
                                remining={remining}
                                setRemining={setRemining}
                            /> :
                                isAutoSelect ? <Row className="items-show">
                                    <Col md={3}>
                                        <FormGroup>
                                            <ControlLabel>Item</ControlLabel>
                                            <Select
                                                clearable={false}
                                                placeholder={""}
                                                value={selectedItem}
                                                options={AllItems}
                                                onChange={data => {
                                                    if (data) {
                                                        setSelectedType([])
                                                        setSelectedItem(data)
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ControlLabel>Type</ControlLabel>
                                            <Select
                                                clearable={false}
                                                placeholder={""}
                                                value={selectedType}
                                                options={selectedItem.types ? selectedItem.types : []}
                                                onChange={data => {
                                                    if (data) {
                                                        var newPriceArray = data.type.price.map(pr => {
                                                            return { label: pr.particular, value: pr.particular, price: pr }
                                                        })
                                                        setSelectedPrice([])
                                                        setPriceArray(newPriceArray)
                                                        setSelectedType(data)
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Particular</ControlLabel>
                                            <Select
                                                clearable={false}
                                                placeholder={""}
                                                value={selectedPrice}
                                                options={priceArray}
                                                onChange={data => {
                                                    setSelectedPrice(data)
                                                }
                                                }
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Price</ControlLabel>
                                            <input
                                                className="form-control"
                                                value={selectedPrice.price ? selectedPrice.price.price : 0}
                                                readOnly={true}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Discount</ControlLabel>
                                            <input
                                                className="form-control"
                                                value={discount}
                                                onChange={(e) => setDiscount(e.target.value)}
                                                type='number'
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Count</ControlLabel>
                                            <input
                                                onKeyPress={event => {
                                                    if (event.which == 13 || event.keyCode == 13) {
                                                        if (selectedPrice.price)
                                                            handleOnClick()
                                                    }
                                                }}
                                                type={"number"}
                                                className="form-control"
                                                value={count}
                                                onChange={e => setCount(e.target.value)}
                                                readOnly={!selectedPrice.price}
                                            />
                                        </FormGroup>
                                    </Col>

                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Sub Total</ControlLabel>
                                            <input
                                                type={"number"}
                                                className="form-control"
                                                value={selectedPrice.price ? (selectedPrice.price.price - (discount === "" ? 0 : discount)) * count : 0}
                                                readOnly={true}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Add to List</ControlLabel>
                                            <Button
                                                disabled={!selectedPrice.price}
                                                onClick={() => {

                                                    handleOnClick()
                                                }}
                                                className="btn-submit" fill type="submit">
                                                <i className={"fa fa-plus"} />Add</Button>
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel>Short Key</ControlLabel>
                                            <input
                                                onKeyPress={event => {
                                                    if (event.which == 13 || event.keyCode == 13) {
                                                        if (selectedPrice.price)
                                                            handleOnClick()
                                                    }
                                                }}
                                                type={"number"}
                                                className="form-control"
                                                value={shortKey}
                                                onChange={e => setShortKey(e.target.value)}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row> : <Row className="items-show">
                                    <CustomEntry
                                        receiptArray={receiptArray}
                                        setReceiptArray={setReceiptArray}
                                    />
                                </Row>
                        }
                    />
                    <br />
                    <Row className="setting_bar">
                        <Col md={2}>
                            <Button
                                title="Refresh"
                                onClick={() => onGetAllItems()}
                                className="btn-submit" fill>
                                <i className={"pe-7s-refresh"} />
                            </Button>
                        </Col>
                        <Col md={2}>
                            <Switch
                                onChange={() => setIsAutoSelect(!isAutoSelect)}
                                value={isAutoSelect}
                                onText="Auto"
                                offText="Default"
                            />
                        </Col>
                        <Col md={2}>
                            <Switch
                                onChange={() => setIsPayment(!isPayment)}
                                value={isPayment}
                                onText="ON"
                                offText="OFF"
                            />
                        </Col>
                        <Col md={2}>
                            <Button
                                title="Adding Recipts"
                                onClick={() => setShowReciptModal(true)}
                                className="btn-submit" fill>
                                <i className={"pe-7s-plus"} />
                            </Button>
                        </Col>
                        <Col md={2}>
                            <Button
                                title="Read BarCode"
                                onClick={() => setShowReadBarCodeModal(true)}
                                className="btn-submit" fill>
                                <i className={"pe-7s-plus"} />
                            </Button>
                        </Col>
                        <Col md={2}>
                            <Button
                                title="Read BarCode"
                                onClick={(e) => {
                                    if (!listening)
                                        listen(e)
                                    else if(listening)
                                        stop(e)

                                }}
                                className="btn-submit" fill>
                                Speach
                            </Button>
                        </Col>
                    </Row>
                </Col>
                <Col md={4}>
                    <Card
                        title={props.match.params.name + " (" + serialNumber.serialNumber + ")"}
                        content={
                            <div className="recipt-show">
                                <Table responsive>
                                    <thead>

                                        <tr>
                                            <th>روپے</th>
                                            <th>نرخ</th>
                                            <th>تفسیل</th>
                                            <th>تعداد</th>
                                            <th style={{ textAlign: "center" }}>#</th>
                                            <th>X</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <Fragment>
                                            {
                                                receiptArray.map((element, key) => {
                                                    return <tr key={key}>
                                                        <th>{element.subTotal}</th>
                                                        <th>{element.price}</th>
                                                        <th>{element.type === element.item ? element.item : (element.item + " " + element.type)}</th>
                                                        <th>{element.count}</th>
                                                        <th style={{ textAlign: "center" }}>{key + 1}</th>
                                                        <th onClick={() => {
                                                            var newArray = [...receiptArray]
                                                            var newDeductionArray = deductionArray.filter(ded =>
                                                                ded.typeId !== newArray[key].typeId)
                                                            setDeductionArray(newDeductionArray)
                                                            newArray.splice(key, 1)
                                                            setReceiptArray(newArray)
                                                        }}>
                                                            < i style={{
                                                                color: "red", cursor: "pointer"
                                                            }} className={"fa fa-trash"} />
                                                        </th>
                                                    </tr>

                                                })
                                            }

                                        </Fragment>
                                    </tbody>
                                </Table>


                            </div>
                        }
                    />
                    {
                        receiptArray.length || onSubTotal > 0 ?
                            <Row><Col md={8}>
                                <FormGroup>
                                    <input
                                        style={{ fontWeight: 'bold', color: '#ec370e', fontSize: '25px' }}
                                        type={"number"}
                                        className="form-control"
                                        value={receiptArray.length ? (subTotal) : (onSubTotal)}
                                        readOnly={true}
                                    />
                                </FormGroup>
                            </Col>
                                <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>ٹوٹل</Col>

                            </Row> : null
                    }
                    {
                        receiptArray.length <= 0 && onSubTotal > 0 && !remining && !payment ?
                            <div>
                                <Row><Col md={8}>
                                    <FormGroup>
                                        <input
                                            style={{ fontWeight: "bold", fontSize: "22px" }}
                                            type={"number"}
                                            className="form-control"
                                            value={recivedAmmount}
                                            onChange={e => e.target.value !== " " && setRecivedAmmount(e.target.value)}
                                        />
                                    </FormGroup>
                                </Col>
                                    <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>وصول</Col>

                                </Row>

                                <Row><Col md={8}>
                                    <FormGroup>
                                        <input
                                            style={{ fontWeight: "bold", fontSize: "22px" }}
                                            type={"number"}
                                            className="form-control"
                                            value={(recivedAmmount - onSubTotal) < 0 ? 0 : recivedAmmount - onSubTotal}
                                            readOnly={true}
                                        />
                                    </FormGroup>
                                </Col>
                                    <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>بقایا</Col>

                                </Row>
                            </div>
                            : null}
                    {
                        remining ? <Row><Col md={8}>
                            <FormGroup>
                                <input
                                    style={{ fontWeight: 'bold' }}
                                    type={"number"}
                                    className="form-control"
                                    value={remining}
                                    readOnly={true}
                                />
                            </FormGroup>
                        </Col>
                            <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>سابقہ</Col>

                        </Row> : null
                    }
                    {
                        payment ? <Row><Col md={8}>
                            <FormGroup>
                                <input
                                    style={{ fontWeight: 'bold' }}
                                    type={"number"}
                                    className="form-control"
                                    value={payment}
                                    readOnly={true}
                                />
                            </FormGroup>
                        </Col>
                            <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>وصول</Col>

                        </Row> : null
                    }
                    {
                        remining || payment ? <Row><Col md={8}>
                            <FormGroup>
                                <input
                                    style={{ fontWeight: 'bold', fontSize: '25px', color: '#ec370e' }}
                                    type={"number"}
                                    className="form-control"
                                    value={onCalculateSubTotal()}
                                    readOnly={true}
                                />
                            </FormGroup>
                        </Col>
                            <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>سب ٹوٹل</Col>

                        </Row> : null
                    }

                    <Row>
                        <Col md={4}>
                            <Print
                                onSuccessPrint={onSuccessPrint}
                                onBeforePrint={onBeforePrint}
                                allItems={AllItems}
                                receiptArray={receiptArray}
                                date={dateFormate2(new Date())}
                                clientName={props.match.params.name}
                                serialNumber={serialNumber.serialNumber}
                                isPendingPrint={isPendingPrint}
                                setIspendingPrint={setIspendingPrint}
                                payment={payment}
                                remining={remining}
                            />
                        </Col>
                        <Col md={2}>
                            <Button
                                disabled={!receiptArray.length || isPendingPrint}
                                onClick={onRemotePrint}
                                className="btn-submit" fill type="submit"><i className={"fa fa-print"} />
                                {"Remote Print"}
                            </Button>
                        </Col>

                    </Row>
                </Col>
            </Row>
            {
                showAddingReciptModal &&
                <AddReciptModal
                    show={showAddingReciptModal}
                    hide={() => setShowReciptModal(false)}
                />
            }
            {
                showReadBarCodeModal &&
                <ReadBarCodeModal
                    show={showReadBarCodeModal}
                    hide={() => setShowReadBarCodeModal(false)}
                    readBarCode={readBarCode}
                    setReadBarCode={readBarCode}
                    barCodeArray={barCodeArray}
                    setBarCodeArray={setBarCodeArray}
                    receiptArray={receiptArray}
                    setReceiptArray={setReceiptArray}
                />
            }
            {
                notificationAlert &&
                <DeleteAlert
                    hide={() => setNotificationAlert(false)}
                    onDelete={handleOnAdd}
                    text={"آپ  کے  پاس یہ  ایٹم  موجود  نہی  ہے"}
                    buttonText={"جاری  رکے"}
                />
            }
            <BarcodeReader
                onError={(err) => { }}
                onScan={handleBarCode}
            />
        </Grid>
    )
}

const mapDispatchToProps = dispatch => {
    return {
      onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
    }
  }
export default connect(null,mapDispatchToProps) (ClientReceipt);