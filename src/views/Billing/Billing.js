import React, { Fragment } from 'react'
import { filterCaseInsensitive, paramsDateFormet,CustomDateFormate } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { deleteSellsMenDetails, sillsMenDetails,getAllSellsMen } from 'api/api'
import LoginAlert from '../../components/Modals/loginAlert'
import { Col, FormGroup, Row, ControlLabel } from 'react-bootstrap'
import { connect } from 'react-redux'
import AddPayment from 'components/Modals/AddPayment'
import AddBill from 'components/Modals/AddBill'
import Datetime from "react-datetime";
import UpdatePaymentModal from 'components/Modals/updatePayment'
class ClientsDetails extends React.Component {

    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            addPaymentModal: false,
            addBillModal: false,
            date: new Date(),
            allSellsMen:[],
            allPayment:0,
            allBilling:0,
            showUpdatePaymentModal:false
        }
        this.getDetails()
    }
 
    getDetails = async () => {
        try {
            const res = await sillsMenDetails(paramsDateFormet(this.state.date))
            const allSellsMen = await getAllSellsMen()
               var setAllPayment=0;
               var setAllBilling=0;
               res.data.map(itm=>{
                   if(itm.isPayed)
                   setAllPayment+=itm.payment
                   else
                   setAllBilling+=itm.payment
               })
            this.setState({allPayment:setAllPayment,allBilling:setAllBilling, Data: res.data,allSellsMen:allSellsMen.data })
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    onDeleteClientDetails = async () => {
        try {
            const {_id,payment,isPayed,name}=this.state.selectedObj
            await deleteSellsMenDetails(_id,payment,isPayed,name._id)
            this.getDetails()
        } catch (err) {

        }
    }
    render() {
        const {showUpdatePaymentModal, allPayment,allBilling, allSellsMen,date, addBillModal, addPaymentModal, Data, loading, deleteAlert, selectedObj } = this.state
        const { onShowNotification } = this.props
        const data = Data.map((prop, sn) => {
            return {
                sn: sn + 1,
                name: prop.name.name,
                distribution:prop.name.item,
                discription:prop.discription,
                payed: prop.isPayed ? "payment" : <span className="red-color">Bill</span>,
                ammount: prop.isPayed?prop.payment:<span className="red-color">{prop.payment}</span>,
                date: CustomDateFormate(prop.date,"DD/MM/YYYY"),
                actions: <div className="table-action-btn-wrapper">

                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showUpdatePaymentModal: true }))
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ deleteAlert: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />

                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: 'name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: 'distribution',
                accessor: 'distribution',
                filterable: true,
            },
            {
                Header: "date",
                accessor: "date",
                filterable: false,
            },
            {
                Header: "payed",
                accessor: "payed",
                filterable: false,
            },
            {
                Header: "discription",
                accessor: "discription",
                filterable: false,
                Footer: (
                    <span >
                      <b style={{ color: '#00e08d' }}>
                    Billing: {                          // Get the total of the price
                         allBilling
                        }</b></span>
                  ),
            },
            {
                Header: "ammount",
                accessor: "ammount",
                filterable: false,
                Footer: (
                    <span >
                      <b style={{ color: 'white' }}>
                       Payed: {                          // Get the total of the price
                         allPayment
                        }</b></span>
                  ),
            },

            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row style={{ textAlign: "right" }}>
                    <Col md={4}>
                        <Datetime
                            timeFormat={false}
                            inputProps={{ placeholder: "Select date" }}
                            defaultValue={new Date()}
                            onChange={(d) => this.setState({ date: d })}
                            value={date}
                            onBlur={this.getDetails}
                        />
                    </Col>
                    <Col md={4}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addPaymentModal: true })}>
                            <i className={"fa fa-plus"} />Add Payment</Button>
                    </Col>
                    <Col md={4}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addBillModal: true })}>
                            <i className={"fa fa-plus"} />Add Bill</Button>
                    </Col>
                </Row>
                <div className="client-details">
                    <ReactTable
                        title={"test"}
                        data={data}
                        filterable
                        columns={Columns}
                        defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                        // manual
                        defaultPageSize={10}
                        // onFetchData={onFetch}
                        showPaginationBottom
                        showPaginationTop={false}
                        // pages={this.state.pages}
                        loading={loading}
                        sortable={false}
                        className="-striped -highlight"
                    />
                </div>

                {
                    deleteAlert &&
                    <LoginAlert
                        hide={() => this.setState({ deleteAlert: false })}
                        show={deleteAlert}
                        onDelete={()=>this.setState({deleteAlert:false},()=>this.onDeleteClientDetails())}
                    />
                }

                {addPaymentModal &&
                    <AddPayment
                        show={addPaymentModal}
                        hide={() => this.setState({ addPaymentModal: false })}
                        fetchData={this.getDetails}
                        allData={allSellsMen}
                        onSowNotification={onShowNotification}

                    />
                }
                 {showUpdatePaymentModal &&
                    <UpdatePaymentModal
                        show={showUpdatePaymentModal}
                        hide={() => this.setState({ showUpdatePaymentModal: false })}
                        fetchData={this.getDetails}
                        selectedObj={selectedObj}
                        onSowNotification={onShowNotification}

                    />
                }
                {addBillModal &&
                    <AddBill
                        show={addBillModal}
                        hide={() => this.setState({ addBillModal: false })}
                        fetchData={this.getDetails}
                        allData={allSellsMen}
                        onSowNotification={onShowNotification}

                    />
                }


            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
    }
}
export default connect(null, mapDispatchToProps)(ClientsDetails)