import React, { useState, useEffect, Fragment } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Row, Col, Table, Badge } from 'react-bootstrap'
import useForm from "react-hook-form";
import { getItems, addItem, deleteItem } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
import AddItemTypeModal from 'components/Modals/addItemType'
import EditItemTypeModal from 'components/Modals/editItemType'
import EditItemModal from 'components/Modals/editItemModal'
import DeleteAlert from '../../components/Modals/deleteAlert'

const AddProducts = ({ show, hide, fetchData, onSowNotification }) => {
    const { handleSubmit, register, errors, reset } = useForm();
    const [allProducts, setAllProducts] = useState([])
    const [filterProducts, setFilterProducts] = useState([])
    const [addItemLoading, setAddItemLoading] = useState(false)
    const [selectedObj, setSelectedObj] = useState('')
    const [showAddItemTypeModal, setShowAddItemTypeModal] = useState(false)
    const [showEditTypeModal, setShowEditTypeModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState('')
    const [particulars, setParticulars] = useState([])
    const [showEditItemModal, setShowEditItemModal] = useState(false)
    const [filterValue, setFilterValue] = useState('')
    const [deleteItemAlert, setDeleteItemAlert] = useState(false)
    useEffect(() => {
        onGetItems()
    }, [])
    const onDeleteItem = async () => {
        try {
            await deleteItem(selectedObj._id)
            onGetItems()
        } catch (err) {
            console.log(err)
        }
    }
    const onSubmit = async (value) => {
        const data = {
            name: value.product,
            particulars: particulars.filter(fil => fil !== '')
        }

        try {
            setAddItemLoading(true)
            await addItem(data)
            reset({
                product: ''
            })
            setParticulars([])
            setAddItemLoading(false)
            onGetItems()
        } catch (err) {
            setAddItemLoading(false)
        }
    }
    const onGetItems = async () => {
        try {
            const res = await getItems()
            setAllProducts(res.data)
            if (filterValue !== "") {
                const newFilter = res.data.filter(val => val.name.startsWith(filterValue) == true)
                setFilterProducts(newFilter)
            }
            else
                setFilterProducts(res.data)
        } catch (err) {

        }
    }
    const onFilter = (e) => {
        setFilterValue(e.target.value)
        const newFilter = allProducts.filter(val => val.name.startsWith(e.target.value) == true)
        setFilterProducts(newFilter)
    }
    return (
        <Grid fluid>

            <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                <Row>
                    <Col md={4}>
                        <FormGroup
                            validationState={errors.product && errors.product.message ? "error" : "success"}
                        >
                            <ControlLabel>New Item</ControlLabel>
                            <input

                                name="product"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Item Name"
                                className="form-control"

                            />
                            {(errors.product && errors.product.message) && <small className="text-danger">{errors.product && errors.product.message}</small>}
                        </FormGroup>
                    </Col>
                    <Col md={2}>
                        <ControlLabel>Particular</ControlLabel>
                        <input
                            name="product"
                            value={particulars[0]}
                            placeholder="Enter Item Name"
                            className="form-control"
                            onChange={e => setParticulars([particulars[particulars.length] = e.target.value])}
                        />
                    </Col>
                    {
                        particulars.map((part, key) => (
                            <Col md={2}>
                                <ControlLabel>Particular</ControlLabel>
                                <input
                                    name="product"
                                    value={particulars[key + 1]}
                                    onChange={e => {
                                        var newParticular = [...particulars]
                                        newParticular[key + 1] = e.target.value
                                        setParticulars(newParticular)
                                    }}
                                    placeholder="Enter Item Name"
                                    className="form-control"

                                />
                            </Col>
                        ))
                    }

                    <Col md={2} className="attendance-header-wrapper-btn">
                        <ControlLabel></ControlLabel>
                        <Button
                            disabled={addItemLoading}
                            className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>
                    </Col>
                </Row>
            </Form>
            <Table responsive className="custom-table-wrapper" >
                <thead >
                    <tr>
                        <th className="text-center">#</th>
                        <th style={{ width: "200px" }}>Item NAME
                  <input value={filterValue} onChange={onFilter} type="text" className="form-control" />
                        </th>
                        <th>types</th>
                        <th className="text-right">ACTiON</th>
                    </tr>
                </thead>
                <tbody>
                    {filterProducts.map((itm, key) => (
                        <tr key={key}>
                            <td className="text-center">{key + 1}</td>
                            <td style={{
                                fontSize:'20px',
                                fontWeight:'bold'
                            }}>{itm.name}</td>
                            <td>
                                {
                                    itm.types.map((typ, k) => (
                                        <Fragment>
                                            <Badge style={{ cursor: 'pointer',fontSize:'16px',padding:'6px',background:'blue' }} pill variant="secondary" onClick={() => {
                                                setSelectedObj(typ)
                                                setSelectedItem(itm)
                                                setShowEditTypeModal(true)
                                            }}>
                                                {typ.name}
                                            </Badge>{' '}
                                        </Fragment>
                                    ))
                                }

                            </td>
                            <td className="text-right table-action-btn-wrapper"><div className="inline-item">
                                <Button onClick={() => {
                                    setSelectedObj(itm)
                                    setShowAddItemTypeModal(true)
                                }} title="Add type" simple bsStyle="info" bsSize="xs" fill>
                                    <i className="fa fa-plus" />
                                </Button>
                                <Button onClick={() => {
                                    setSelectedObj(itm)
                                    setShowEditItemModal(true)
                                }} title="Edit Type" simple bsStyle="success" bsSize="xs" fill>
                                    <i className="fa fa-pencil" />
                                </Button>
                                <Button disabled={itm.types.length} onClick={() => {
                                    setSelectedObj(itm)
                                    setDeleteItemAlert(true)
                                }} title="Delete Item" simple bsStyle="danger" bsSize="xs" fill>
                                    <i className="fa fa-trash" />
                                </Button>
                            </div>

                            </td>
                        </tr>
                    ))}


                </tbody>
            </Table>


            {
                showAddItemTypeModal &&
                <AddItemTypeModal
                    show={showAddItemTypeModal}
                    hide={() => {
                        setShowAddItemTypeModal(false)
                        onGetItems()
                    }}
                    selectedObj={selectedObj}
                />
            }

            {
                showEditTypeModal &&
                <EditItemTypeModal
                    show={showEditTypeModal}
                    hide={() => setShowEditTypeModal(false)}
                    selectedObj={selectedObj}
                    itemName={selectedItem.name}
                    particulars={selectedItem.particulars}
                    fetchData={() => {
                        onGetItems()
                        fetchData()
                    }}
                />
            }
            {
                showEditItemModal &&
                <EditItemModal
                    show={showEditItemModal}
                    hide={() => setShowEditItemModal(false)}
                    selectedObj={selectedObj}
                    onGetItems={onGetItems}
                />
            }
            {
                deleteItemAlert &&
                <DeleteAlert
                    hide={() => setDeleteItemAlert(false)}
                    onDelete={onDeleteItem}
                />
            }
        </Grid>
    )
}

export default AddProducts