import React,{Fragment} from 'react'
import { dateFormate, filterCaseInsensitive } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { getItemsTypes, deleteItemType } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import EditItemTypeModal from 'components/Modals/editItemType'
import { Row,Badge } from 'react-bootstrap'
import ShortKey from 'components/Modals/shortKeyModal'
import { connect } from 'react-redux'

class Products extends React.Component {

    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            showEditTypeModal:false,
            showShortKeyModal:false
        }
        this.getProducts()
    }

    getProducts = async () => {
        try {
            const res = await getItemsTypes()
            this.setState({ Data: res.data })
        } catch (err) {

        }
    }
    onDeleteProductType = async () => {
        try {
            await deleteItemType(this.state.selectedObj._id, this.state.selectedObj.itemId._id)
            this.getProducts()
        } catch (err) {

        }
    }
    render() {
        const { showShortKeyModal,Data, loading, deleteAlert,showAddProductModal,showEditTypeModal,selectedObj } = this.state
        const data = Data.map((prop, sn) => {

            return {
                sn: sn + 1,
                type: prop.name,
                product: prop.itemId.name,
                price: <Fragment>
                    {prop.price.map((typ, k) => (
                      <Fragment>
                        <Badge pill variant="secondary" >
                          {typ.particular+" - "+typ.price}
                        </Badge>{' '}
                      </Fragment>
                    ))}
                    </Fragment>,
                totalCount: prop.count.totalCount+" ,"+prop.count.particular,
                shortKey:prop.shortKey?prop.shortKey:'-',
                lastUpdate: dateFormate(prop.updatedAt),
                actions: <div className="table-action-btn-wrapper">
                     <Button onClick={() => {
                      this.setState({selectedObj:prop},()=>this.setState({showShortKeyModal:true}))
                    }} title="View" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-check"} />
                    </Button>
                    <Button onClick={() => {
                      this.setState({selectedObj:prop},()=>this.setState({showEditTypeModal:true}))
                    }} title="View" bsStyle="primary"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>

                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () =>this.setState({deleteAlert:true}))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />
                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: "product",
                accessor: "product",
                filterable: true,
                id: "product"
            },
            {

                Header: "type",
                accessor: "type",
                filterable: true,
                id: "type"
            },
            {

                Header: "price",
                accessor: "price",
                filterable: false,
                id: "price"
            },
            {

                Header: "total count",
                accessor: "totalCount",
                filterable: false,
                id: "totalCount"
            },
            {

                Header: "short key",
                accessor: "shortKey",
                filterable: true,
                id: "shortKey"
            },

            {

                Header: "last update",
                accessor: "lastUpdate",
                filterable: false,
                id: "lastUpdate"
            },



            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },



        ]
        return (

            <div className="react-table-custom-design">
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />

                {
                    deleteAlert &&
                    <DeleteAlert
                        hide={() => this.setState({deleteAlert:false})}
                        onDelete={this.onDeleteProductType}
                    />
                }
               
                {
                    showEditTypeModal&&
                    <EditItemTypeModal
                    show={showEditTypeModal}
                    hide={()=>this.setState({showEditTypeModal:false})}
                    selectedObj={selectedObj}
                    itemName={selectedObj.itemId.name}
                    fetchData={this.getProducts}
                    particulars={selectedObj.itemId.particulars}
                    />
                }
                {
                    showShortKeyModal&&
                    <ShortKey
                    show={showShortKeyModal}
                    hide={()=>this.setState({showShortKeyModal:false})}
                    selectedObj={selectedObj}
                    onShowNotification={this.props.onShowNotification}
                    onFetch={this.getProducts}
                    />
                }
            </div>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
  }
export default connect(null,mapDispatchToProps) (Products)