import React, { Fragment } from 'react'
import {  filterCaseInsensitive } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { deleteClientName,updateClientName,AllClientName } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import { Col,FormGroup,Row,ControlLabel } from 'react-bootstrap'
import AddDetailsModal from 'components/Modals/AddClientDetails'
import EditDetailsModal from 'components/Modals/EditClientDetails'
class ClientsDetails extends React.Component {

    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            showEditModal: false,
            showAddDetailsModal:false
        }
        this.getDetails()
    }

    getDetails = async () => {
        try {
            const res = await AllClientName()
            this.setState({ Data: res.data })
        } catch (err) {

        }
    }
    onDeleteClientDetails = async () => {
        try {
            await deleteClientName(this.state.selectedObj._id)
            this.getDetails()
        } catch (err) {

        }
    }
    render() {
        const { showAddDetailsModal,Data, loading, deleteAlert, selectedObj,showEditModal } = this.state
        const data = Data.map((prop, sn) => {

            return {
                sn:sn+1,
               name:prop.name?prop.name:'-',
               address:prop.address?prop.address:'-',
               contactNumber:prop.contactNumber?prop.contactNumber:'-',
                actions: <div className="table-action-btn-wrapper">
                    
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showEditModal: true }))
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ deleteAlert: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />

                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: 'name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: "address",
                accessor: "address",
                filterable: true,
            },
            {
                Header: "contact number",
                accessor: "contactNumber",
                filterable: true,
            },
            
            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row style={{textAlign:"right"}}>
                    <Col md={12}>
                    <Button className="btn-submit" fill onClick={()=>this.setState({showAddDetailsModal:true})}>
                        <i className={"fa fa-plus"} />Add New Record</Button>
                    </Col>
                </Row>
                <div className="client-details">
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />
                </div>

                {
                    deleteAlert &&
                    <DeleteAlert
                        hide={() => this.setState({ deleteAlert: false })}
                        onDelete={this.onDeleteClientDetails}
                    />
                }
                {
                    showAddDetailsModal&&
                    <AddDetailsModal
                    show={showAddDetailsModal}
                    hide={()=>this.setState({showAddDetailsModal:false})}
                    fetchData={this.getDetails}
                    />
                }
                {showEditModal&&
                <EditDetailsModal
                show={showEditModal}
                hide={()=>this.setState({showEditModal:false})}
                fetchData={this.getDetails}
                selectedObj={selectedObj}
                />
                }
               

            </div>
        )
    }
}

export default ClientsDetails