import React, { Fragment } from 'react'
import { filterCaseInsensitive, CustomDateFormate,calCulateTotalAmmount } from 'utils/helper'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { deleteSellsMenDetails, sillsMenDetailsById, getAllSellsMen,updateSellsMen } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import { Col, FormGroup, Row, ControlLabel } from 'react-bootstrap'
import { connect } from 'react-redux'
import AddPayment from 'components/Modals/AddPayment'
import AddBill from 'components/Modals/AddBill'
import PrintSillsMen from 'components/printSillsMen'
import UpdatePaymentModal from 'components/Modals/updatePayment'
class ClientsDetails extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            addPaymentModal: false,
            addBillModal: false,
            allSellsMen: [],
            showUpdatePaymentModal:false,
            calculateAmmount:[]
        }
        this.getDetails()
    }

    updateSillsMen=async()=>{
        try{
            const data={
                ammount:this.state.calculateAmmount[this.state.calculateAmmount.length-1]
            }
           await updateSellsMen(this.props.match.params.id,data)
        }catch(err){
        }
    }
    getDetails = async () => {
        try {
            const res = await sillsMenDetailsById(this.props.match.params.id)
            const allSellsMen = await getAllSellsMen()
            this.setState({ Data: res.data, allSellsMen: allSellsMen.data,calculateAmmount:calCulateTotalAmmount(res.data) })
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    onDeleteClientDetails = async () => {
        try {
            const { _id, payment, isPayed, name } = this.state.selectedObj
            await deleteSellsMenDetails(_id, payment, isPayed, name._id)
            this.getDetails()
        } catch (err) {

        }
    }
    render() {
        const { calculateAmmount,showUpdatePaymentModal,allSellsMen, addBillModal, addPaymentModal, Data, loading, deleteAlert, selectedObj } = this.state
        const { onShowNotification } = this.props
        const data = Data.map((prop, sn) => {
            return {
                sn: sn + 1,
                name: prop.name.name,
                discription:prop.discription,
                payed: prop.isPayed ? "payment" : <span className="red-color">Bill</span>,
                ammount: prop.isPayed ? prop.payment : <span className="red-color">{prop.payment}</span>,
                date: CustomDateFormate(prop.date,"DD/MM/YYYY"),
                total:calculateAmmount[sn],
                actions: <div className="table-action-btn-wrapper">

                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showUpdatePaymentModal: true }))
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={"fa fa-pencil"} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ deleteAlert: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={"fa fa-trash"} />

                    </Button>
                </div>
            };
        })


        const Columns = [
            {
                Header: "#",
                accessor: "sn",
                filterable: false,
            },
            {
                Header: 'name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: "date",
                accessor: "date",
                filterable: false,
            },
            {
                Header: "payed",
                accessor: "payed",
                filterable: false,
            },
            {
                Header: "discription",
                accessor: "discription",
                filterable: false,
            },
            {
                Header: "ammount",
                accessor: "ammount",
                filterable: false,
                Footer: (
                    <span >
                      <b style={{ color: 'white' }}>
                       Total: {                          // Get the total of the price
                         Data.length>0?Data[0].name.ammount:0
                        }</b></span>
                  ),
            },
            {
                Header:"total",
                accessor:"total",
                filterable:false
            },

            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row style={{ textAlign: "right" }}>

                    <Col md={3}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addPaymentModal: true })}>
                            <i className={"fa fa-plus"} />Add Payment</Button>
                    </Col>
                    <Col md={3}>
                        <Button className="btn-submit" fill onClick={() => this.setState({ addBillModal: true })}>
                            <i className={"fa fa-plus"} />Add Bill</Button>
                    </Col>
                    <Col md={3}>
                    <PrintSillsMen data={Data}/>
                    </Col>
                    <Col md={3}>
                    <Button className="btn-submit" fill onClick={() => {
                        if(this.state.calculateAmmount.length>=1)
                        this.updateSillsMen()
                        }}>
                            <i className={"fa fa-plus"} />update payment</Button>
                    </Col>
                </Row>
                <div className="client-details">
                    <ReactTable
                        title={"test"}
                        data={data}
                        filterable
                        columns={Columns}
                        defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                        // manual
                        defaultPageSize={10}
                        // onFetchData={onFetch}
                        showPaginationBottom
                        showPaginationTop={false}
                        // pages={this.state.pages}
                        loading={loading}
                        sortable={false}
                        className="-striped -highlight"
                    />
                </div>

                {
                    deleteAlert &&
                    <DeleteAlert
                        hide={() => this.setState({ deleteAlert: false })}
                        onDelete={this.onDeleteClientDetails}
                    />
                }

                {addPaymentModal &&
                    <AddPayment
                        show={addPaymentModal}
                        hide={() => this.setState({ addPaymentModal: false })}
                        fetchData={this.getDetails}
                        allData={allSellsMen}
                        onSowNotification={onShowNotification}
                        isSelectToPay={true}
                        selectedObj={Data.length ? {
                            name: Data[0].name.name,
                            _id: Data[0].name._id,
                            item: Data[0].name.item
                        } : {}}

                    />
                }
                {addBillModal &&
                    <AddBill
                        show={addBillModal}
                        hide={() => this.setState({ addBillModal: false })}
                        fetchData={this.getDetails}
                        allData={allSellsMen}
                        onSowNotification={onShowNotification}
                        isSelectToPay={true}
                        selectedObj={Data.length ? {
                            name: Data[0].name.name,
                            _id: Data[0].name._id,
                            item: Data[0].name.item
                        } : {}}

                    />
                }
                 {showUpdatePaymentModal &&
                    <UpdatePaymentModal
                        show={showUpdatePaymentModal}
                        hide={() => this.setState({ showUpdatePaymentModal: false })}
                        fetchData={this.getDetails}
                        selectedObj={selectedObj}
                        onSowNotification={onShowNotification}

                    />
                }


            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
    }
}
export default connect(null, mapDispatchToProps)(ClientsDetails)