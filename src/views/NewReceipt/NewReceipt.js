import React,{useState,useEffect} from 'react'
import Button from "components/CustomButton/CustomButton.jsx";
import { Row, Col, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import Card from 'components/Card/Card'
import { useSpeechSynthesis ,useSpeechRecognition } from 'react-speech-kit';
import { AllClientName } from 'api/api'
import Select from "react-select";

const Receipt = (props) => {
  useEffect(()=>{
    onGetClientsName()
  },[])
  const [value, setValue] = useState('');
  const { listen, listening, stop } = useSpeechRecognition({
    onResult: (result) => {
      setValue(result);
    },
  });
  const [AllClients,setAllClients]=useState([])
  const [selectedClient,setSelectedClient]=useState([])
  const [name,setName]=useState('')
  const [isValid,setIsValid]=useState(true)
   const onGetClientsName=async()=>{
       var data=await AllClientName()
        var newData=data.data.map(client=>{
            return{label:client.name,value:client.name}
        })
        setAllClients(newData)
   }
    const onSubmit = (e) => {
        e.preventDefault()
        var newName=''
        if(name===""&&!selectedClient.label){
            setIsValid(false)
            return
        }
        if(name!=="")
        newName=name
        if(selectedClient.label)
        newName=selectedClient.label
      window.open(`/account/ClientReceipt/${newName}`)
    }
  const { speak } = useSpeechSynthesis();
    return (
        <div className="client_receipt_wrapper">
            <Row>
            <Col md={8} sm={6} mdOffset={2} smOffset={3}>
                    <Card
                        //hidden={this.state.cardHidden}
                        textCenter
                        title="Client Name"
                        content={
                            <Form onSubmit={onSubmit} autoComplete="off">
                                <Row>
                                    <Col md={6}>
                                        <FormGroup >
                                            {/* <ControlLabel>Client Name</ControlLabel> */}
                                            <input
                                                style={{ fontSize: '20px' }}
                                                value={name}
                                                onChange={(e)=>{
                                                    if(e.target.value!==''&&selectedClient.label)
                                                    setSelectedClient([])
                                                    setName(e.target.value)}}
                                                placeholder="Enter Client Name"
                                                className="form-control"

                                            />
                                        </FormGroup>
                                    </Col>
                                   <Col md={6}>
                                   <FormGroup>
                                        <Select
                                            clearable={false}
                                            placeholder={"Single Select"}
                                            value={selectedClient}
                                            options={AllClients}
                                            onChange={data => {
                                                if (data) {
                                                    setName('')
                                                    setSelectedClient(data)
                                                }
                                            }}
                                        />
                                    </FormGroup>
                                   </Col>
                                </Row>
                                <Row>
                                <Col md={2}>
                                        <ControlLabel></ControlLabel>
                                        <Button
                                            //  disabled={addItemLoading}
                                            className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>
                                                  { !isValid&&<small className="text-danger">Please Select or Enter Client Name</small>}

                                    </Col>
                                </Row>
                            </Form>
                        }
                    />

                </Col>
            </Row>
           
      <button onClick={() => {
                 speak({ text: value })
      }
        }>Speak</button>

<button onMouonseDown={(e)=>listen(e)} onMouseUp={stop}>
        🎤
      </button>
      <h1>{value}</h1>
        </div>
    )
}

export default Receipt