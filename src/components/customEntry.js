import React, { useState } from 'react'
import { Row, Col,FormGroup,ControlLabel } from 'react-bootstrap'
import Button from "components/CustomButton/CustomButton.jsx";

const CustomReciptEntry = ({receiptArray,setReceiptArray}) => {
    const [item, setItem] = useState('')
    const [price, setPrice] = useState('')
    const [count, setCount] = useState(1)

    const handleOnClick=()=>{
       var data={
        count: count,
        item: item,
        particular:'',
        price:price,
        subTotal: price*count,
        type:item,
        typeId:"",
        isCustomEntry:true
       }
       var newArray = [...receiptArray]
       newArray.push(data)
       setReceiptArray(newArray)
       setItem('')
       setPrice('')
       setCount(1)
    }
    return (
        <Row>
            <Col md={3}>
                <FormGroup>
                    <ControlLabel>Item</ControlLabel>
                    <input
                        type={"text"}
                        className="form-control"
                        value={item}
                        onChange={e => setItem(e.target.value)}
                    />
                </FormGroup>
            </Col>

            <Col md={3}>
                <FormGroup>
                    <ControlLabel>Price</ControlLabel>
                    <input
                     onKeyPress={event => {
                        if (event.which == 13 || event.keyCode == 13) {
                            if(price&&count>=1)
                            handleOnClick()
                        }
                    }}
                        type={"number"}
                        className="form-control"
                        value={price}
                        onChange={e => setPrice(e.target.value)}
                    />
                </FormGroup>
            </Col>

            <Col md={2}>
                <FormGroup>
                    <ControlLabel>Count</ControlLabel>
                    <input
                    onKeyPress={event => {
                        if (event.which == 13 || event.keyCode == 13) {
                            if(price&&count>=1)
                            handleOnClick()
                        }
                    }}
                        type={"number"}
                        className="form-control"
                        value={count}
                        onChange={e =>{
                            setCount(e.target.value)
                        }}
                    />
                </FormGroup>
            </Col>

            <Col md={2}>
                <FormGroup>
                    <ControlLabel>Sub Total</ControlLabel>
                    <input
                   
                        type={"number"}
                        className="form-control"
                        value={price * count}
                        readOnly={true}
                    />
                </FormGroup>
            </Col>

            <Col md={2}>
                <FormGroup>
                    <ControlLabel>Add to List</ControlLabel>
                    <Button
                        disabled={item===""||price===""}
                        onClick={handleOnClick}
                        className="btn-submit" fill type="submit">
                        <i className={"fa fa-plus"} />Add
                        </Button>
                </FormGroup>
            </Col>
        </Row>
    )
}
export default CustomReciptEntry