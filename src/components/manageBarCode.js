import React, { useState } from 'react'
import BarcodeReader from 'react-barcode-reader'
import PrintBarCode from 'components/printBarCode'
import Button from "components/CustomButton/CustomButton.jsx";
import { Row,Col,FormGroup,ControlLabel } from 'react-bootstrap'
import NewBarCodeModal from 'components/Modals/newBarCodeModal'
import { connect } from 'react-redux'
const ManageBarCode = (props) => {
  const [showBarCodeModal, setShowBarCodeModal] = useState(false)
  const [readBarCode, setReadBarCode] = useState('')
  const [code, setCode] = useState('')
  const [count, setCount] = useState('')
  const handleBarCode = (data) => {
    setReadBarCode(data)
  }
  return (
    <div>
      <Row>
        <Button
          onClick={() => setShowBarCodeModal(true)}
          className="btn-submit" fill type="submit"><i className={"fa fa-print"} />
          {"add BarCode"}
        </Button>
      </Row>
      <Row>
        <Col md={6}> <FormGroup>
          <ControlLabel>Enter BarCode</ControlLabel>
          <input
            value={code}
            onChange={e => setCode(e.target.value)}
            placeholder="Enter Bar Code Name"
            className="form-control"

          />
        </FormGroup></Col>
        <Col md={6}> <FormGroup>
          <ControlLabel>Count</ControlLabel>
          <input
            type={'number'}
            value={count}
            onChange={e => setCount(e.target.value)}
            placeholder="Enter Count"
            className="form-control"

          />
        </FormGroup></Col>
      </Row>
      <PrintBarCode data={[]} code={code} count={count} />
      <BarcodeReader
        onError={(err) =>{}}
        onScan={handleBarCode}
      />
      {
        showBarCodeModal &&
        <NewBarCodeModal
          show={showBarCodeModal}
          hide={() => {
            setReadBarCode('')
            setShowBarCodeModal(false)
          }}
          onSowNotification={props.onShowNotification}
          readBarCode={readBarCode}
          setReadBarCode={setReadBarCode}
        />
      }
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    onShowNotification: (errorType, errorMessage) => dispatch({ type: "SET_NOTIFICATION", errorType, errorMessage }),
  }
}
export default connect(null, mapDispatchToProps)(ManageBarCode)