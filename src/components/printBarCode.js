import React, { useState, useEffect, Fragment } from 'react'
import Button from "components/CustomButton/CustomButton.jsx";
import { calCulateTotalAmmount } from 'utils/helper'
const qz = require("qz-tray");
const NodeThermalPrinter = ({ data, code, count }) => {
    //barcode data

    //convenience method
    var chr = function (n) { return String.fromCharCode(n); };

    var barcode = '\x1D' + 'h' + chr(80) +   //barcode height
        '\x1D' + 'f' + chr(0) +              //font for printed number
        '\x1D' + 'k' + chr(69) + chr(code.length) + code + chr(0); //code39
    useEffect(() => {

    }, [data])


    const printData = () => {
        var dt = []
        var newCount=+count;
        for (let i = 1; i <= newCount; i++) {
            dt.push('\n' + barcode + '\n')
        }
       
        dt.push('\n')
        dt.push('\n')
        dt.push('\n')
        dt.push('\x1B' + '\x69'+ '\n')
        return dt
    }
    const onGoPrint = async () => {
        try {
            const websocket = await qz.websocket.connect({ retries: 0, delay: 1 })
            let config = await qz.configs.create("BC-95AC")
            await qz.print(config, printData()).then(pr => {
            }).catch(err => {
                alert("err", err)
            })

            await qz.websocket.disconnect()
        }
        catch (err) {
            console.log(err)
        }
    }
    const onPrint = async () => {
        try {
            onGoPrint()
        } catch (err) {
            console.log(err)
        }

    }

    return (
        <div>
            <Button
                onClick={onPrint}
                className="btn-submit" fill type="submit"><i className={"fa fa-print"} />
                {"Print"}
            </Button>
        </div>
    )
}

export default NodeThermalPrinter