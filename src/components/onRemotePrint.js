import React from 'react'
const qz = require("qz-tray");
const NodeThermalPrinter = ({id,serialNumber, receiptArray, date, clientName, onSuccessPrint}) => {
    if(receiptArray)
    onGoPrint(id,serialNumber, receiptArray, date, clientName, onSuccessPrint)

}
const printData = (subTotal,serialNumber, receiptArray, date, clientName) => {
    return [
        {
            type: 'pixel',
            format: 'html',
            flavor: 'plain',
            data: `<html>
            <body>
            <h5 style="text-align:center">03489566419 ------------ رابطہ  نمبر</h5>
            <table style="width: 100%;">
<tr>
<td>
    ${date} :تاریخ
</td>
<td style="text-align: right;">
    ${serialNumber} :بل نمبر
</td>
</tr>
</table>
<h4 style="text-align: center;height:1%;">السیدجنرل سٹور گل مارکیٹ بٹگرام</h4>
<h5 style="text-align: center;height:1%">سیدقاسم علی شاہ -- 03439545645</h5>
<h5 style="text-align: center;height:1%">سیداضخرعلی شاہ -- 03489566419</h5>
<h5 style="text-align: center;height:1%">سید  ملک شاہ ------ 03018123767</h5>

<h5 style="text-align: center;height:1%"> نام: ${clientName}</h5>
            <table style="width:100%;text-align:left;font-size:14px;border:1px solid black">
        <tr>
            <th>روپے</th>
            <th>نرخ</th>
            <th style="text-align:center">تفسیل</th>
            <th>تعداد</th>
        </tr>
        ${receiptArray.map((element, key) => {


                return `<tr>
            <td style="font-weight:bold">${element.subTotal}</td>
            <td style="font-weight:bold">${element.price}</td>
            <td style="text-align: right;font-weight:bold">${element.type === element.item ? (element.particular + " " + element.item) : (element.particular + " " + element.item + " " + element.type)}</td>
            <td style="text-align:right;font-weight:bold">${element.count}</span></td>
        </tr>`

            })

                }
        
    </table>
            
            <h5 style="text-align:center">${subTotal} -------------------ٹوٹل</h5>
                  <p style="text-align:left;font-family: cursive;font-size:8px;font-weight: bold;">Developed by:Uqair Ali</p>
            <br/>
          
              </body>
              </html>`

        }
    ];
}
const onGoPrint = async (id,serialNumber, receiptArray, date, clientName, onSuccessPrint) => {
    try {
 
        await qz.websocket.connect({ retries: 0, delay: 1 })
        let config = await qz.configs.create("BC-95AC")
        
       
        var subTotal = 0;
        receiptArray.map(val => {
            subTotal += val.subTotal
        })
        printData(subTotal,serialNumber, receiptArray, date, clientName).push(); //cut paper
        await qz.print(config, printData(subTotal,serialNumber, receiptArray, date, clientName)).then(successPrt => {
            onSuccessPrint(id)
        }).catch(err => {
            console.log(err)
        });
        await qz.websocket.disconnect()

    }
    catch (err) {
        console.log("errorerror",err)
    }
}

export default NodeThermalPrinter