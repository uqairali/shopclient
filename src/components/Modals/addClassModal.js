import React, { useState } from 'react'
import { Grid, Modal, Form,FormGroup,ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { addClass } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
const AddClass = ({ show, hide,fetchData,onSowNotification }) => {
  const { handleSubmit, register, errors } = useForm();
  const [loading, setLoading] = useState(false)
  const [serverError, setServerError] = useState(null)
  const onSubmit = async (values) => {
    try {
      const data = {
        name: values.name,
        fee: values.fee
      }
      setLoading(true)
      setServerError(null)
      var res=await addClass(data)
      setLoading(false)
      if(res.data){
        fetchData()
        hide()
      }

    } catch (err) {
      setLoading(false)
      setServerError(err.message)
      onSowNotification('error',err.message)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add Class
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup
              validationState={errors.name && errors.name.message ? "error" : "success"}
            >
              <ControlLabel>Class Name</ControlLabel>
              <input
              
                name="name"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Class Name"
                className="form-control"
                
              />
              {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.fee && errors.fee.message ? "error" : "success"}
            >
              <ControlLabel>Fee</ControlLabel>
              <input
                name="fee"
                type="number"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Class Fee"
                className="form-control"
              />
              {(errors.fee && errors.fee.message) && <small className="text-danger">{errors.fee && errors.fee.message}</small>}
            </FormGroup>
              {
                serverError&&
                <div className="center">
                  <small className="text-danger">{serverError}</small>
                </div>
              }

            <div className="center">
              <Button className="btn-submit" fill type="submit"><i className={loading?"fa fa-spin fa-spinner":"fa fa-check"} />Submit</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    
    </Grid>
  )
}

export default AddClass