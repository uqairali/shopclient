import React, { useState, useEffect, Fragment } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Table, Row, Col } from 'react-bootstrap'
import { getTypeByBarCode } from 'api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const ReadingBarCode = ({ receiptArray, setReceiptArray, show, hide, readBarCode, setReadBarCode, barCodeArray, setBarCodeArray }) => {

    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Read BarCode
                     </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>

                        <Col md={12}>
                            <Table responsive>
                                <thead>

                                    <tr>
                                        <th>روپے</th>
                                        <th>نرخ</th>
                                        <th>تفسیل</th>
                                        <th>تعداد</th>
                                        <th style={{ textAlign: "center" }}>#</th>
                                        <th>X</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <Fragment>
                                        {
                                            barCodeArray.map((element, key) => {
                                                return <tr key={key}>
                                                    <th>{element.subTotal}</th>
                                                    <th>{element.price}</th>
                                                    <th>{element.item}</th>
                                                    <th>{element.count}</th>
                                                    <th style={{ textAlign: "center" }}>{key + 1}</th>
                                                    <th onClick={() => {
                                                        var newArray = [...barCodeArray]
                                                        newArray.splice(key, 1)
                                                        setBarCodeArray(newArray)
                                                    }}>
                                                        < i style={{
                                                            color: "red", cursor: "pointer"
                                                        }} className={"fa fa-trash"} />
                                                    </th>
                                                </tr>

                                            })
                                        }

                                    </Fragment>
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Button
                            onClick={() => {
                                setReceiptArray(receiptArray.concat(barCodeArray))
                                setBarCodeArray([])
                            }}
                            className="btn-submit" fill type="submit"><i className={"fa fa-plus"} />
                            {"Submit"}
                        </Button>
                    </Row>
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default ReadingBarCode