import React, { useState, useEffect } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Table, Row, Col } from 'react-bootstrap'
import { editItemType, addNewShortKey,updateShortKey,shortKeyByKey } from 'api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const AddShortKey = ({ show, hide, selectedObj, onFetch, onShowNotification }) => {
    const [shortKey, setShortKey] = useState('')
    const [isEmpty, setIsEmpty] = useState(false)
    const [isUpdate,setIsUpdate]=useState(false)
    useEffect(() => {
        if (selectedObj.shortKey){
            setShortKey(selectedObj.shortKey)
            getShortKey()
        }
    }, [])

    const getShortKey=async()=>{
    try{
        var data=await shortKeyByKey(selectedObj.shortKey)
        if(data.data)
        setIsUpdate(data.data)
    }catch(err){
        onShowNotification("error","Key not Founded!")
    }
    }
    const handleOnClick = async (e) => {
        e.preventDefault()
        setIsEmpty(false)
        if (shortKey === "") {
            setIsEmpty(true)
            return
        }
        try {
            if (isUpdate){
                await updateShortKey(isUpdate._id,{ itemTypeId: selectedObj._id, key: shortKey })
                await editItemType({ shortKey: shortKey }, selectedObj._id)
            }else{
            await addNewShortKey({ itemTypeId: selectedObj._id, key: shortKey })
            await editItemType({ shortKey: shortKey }, selectedObj._id)
            }
            onFetch()
            hide()
            onShowNotification("success","Short Key Added successfully")
        } catch (err) {
            onShowNotification("error","دوسرا  نمبر  درج  کریں")
        }
    }

    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Set Short Key
                     </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleOnClick}>
                        <FormGroup>
                            <ControlLabel>Enter Any Short Key</ControlLabel>
                            <input
                                type="number"
                                className="form-control"
                                value={shortKey}
                                onChange={(e) => setShortKey(e.target.value)}
                            />
                        </FormGroup>
                        {isEmpty && <label style={{ color: 'red', fontWeight: "bold" }}>Please Enter Any Number</label>}
                        <FormGroup>
                            <Button
                                className="btn-submit" fill type="submit">
                                <i className={"fa fa-plus"} />Add</Button>
                        </FormGroup>

                    </Form>

                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default AddShortKey