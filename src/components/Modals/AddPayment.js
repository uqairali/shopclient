import React, { useState, useEffect } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { newSellsMenDetails } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
import Select from "react-select";
import Datetime from "react-datetime";
import { paramsDateFormet } from 'utils/helper';

const AddSellsMen = ({ isSelectToPay, selectedObj, show, hide, fetchData, onSowNotification, allData }) => {
  const [allSellsMen, setAllSellsMen] = useState([])
  const [selectedSellsMen, setSelectedSellsMen] = useState([])
  const [date, setDate] = useState(new Date())
  const [isValidName, setIsValidName] = useState(true)
  useEffect(() => {
    const newData = allData.map(itm => {
      return { label: itm.name, value: itm.name, id: itm._id, item: itm.item }
    })
    if (isSelectToPay) {
      setSelectedSellsMen({
        label: selectedObj.name, value: selectedObj.name,
        id: selectedObj._id, item: selectedObj.item
      })

    } else {
      setAllSellsMen(newData)
    }
  }, [])
  const { handleSubmit, register, errors } = useForm();
  const onSubmit = async (values) => {
    if (!selectedSellsMen.label) {
      setIsValidName(false)
      return
    }
    setIsValidName(true)
    try {
      const data = {
        name: selectedSellsMen.id,
        payment: values.payment,
        discription: values.discription,
        date: paramsDateFormet(date),
        isPayed: true
      }
      var res = await newSellsMenDetails(data)

      if (res.data) {
        fetchData()
        hide()
        onSowNotification('success', "New Record Addedd successfully")
      }

    } catch (err) {
      onSowNotification('error', err.message)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add Payment<span style={{
              fontWeight: 'bold',
              textAlign: 'center',
              color: 'red',
            }} >{selectedSellsMen.label && " (" + selectedSellsMen.label + " " + selectedSellsMen.item + ")"}
            </span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup>
              <ControlLabel>Date</ControlLabel>
              <Datetime
                timeFormat={false}
                inputProps={{ placeholder: "Select date" }}
                defaultValue={new Date()}
                onChange={(d) => setDate(d)}
                value={date}
                closeOnSelect
              />
            </FormGroup>
            <FormGroup>
              <ControlLabel>Select SellsMen</ControlLabel>
              <Select
                clearable={false}
                placeholder={""}
                value={selectedSellsMen}
                options={allSellsMen}
                onChange={data => {
                  if (data) {
                    setSelectedSellsMen(data)
                  }
                }}
              />
              {!isValidName &&
                <small className="text-danger">Please Select Name!</small>}
            </FormGroup>
            <FormGroup
              validationState={errors.payment && errors.payment.message ? "error" : "success"}
            >
              <ControlLabel>Payment</ControlLabel>
              <input

                name="payment"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter payment"
                className="form-control"
                type="number"
              />
              {(errors.payment && errors.payment.message) &&
                <small className="text-danger">{errors.payment && errors.payment.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.discription && errors.discription.message ? "error" : "success"}
            >
              <ControlLabel>Discription</ControlLabel>
              <input
                name="discription"
                ref={register({
                })}
                placeholder="Enter Discription"
                className="form-control"

              />
              {(errors.discription && errors.discription.message) &&
                <small className="text-danger">{errors.discription && errors.discription.message}</small>}
            </FormGroup>

            <div className="center">
              <Button className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

    </Grid>
  )
}

export default AddSellsMen