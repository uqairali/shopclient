import React, { useState, useEffect } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Table, Row, Col } from 'react-bootstrap'
import { ReciptBySerialNumber } from 'api/api'
import { dateFormate } from '../../utils/helper'
import Button from "components/CustomButton/CustomButton.jsx";

const AddingReciptModal = ({ show, hide }) => {
    const [reciptNumber, setReciptNumber] = useState('')
    const [priceArray, setPriceArray] = useState([])
    const [subTotal, setSubTotal] = useState(0)
    const [error, setError] = useState(false)
    useEffect(() => {
        var total= 0
        priceArray.map(el => {
            total += el.total
        })
        setSubTotal(total)
    }, [priceArray])

    const handleOnClick = async (e) => {
        e.preventDefault()
        try {
            setError(false)
            var data = await ReciptBySerialNumber(reciptNumber)
            if (data.data) {
                var newArray = [...priceArray]
                newArray.push({ name: data.data.clientName, serialNumber: data.data.serialNumber, date: data.data.createdAt, total: data.data.totalAmmount })
                setPriceArray(newArray)
                setReciptNumber("")
            } else {
                setError(true)
                setReciptNumber('')
            }
        }
        catch (err) {
            console.log(err)
        }
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Adding Recipts
                     </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Form onSubmit={handleOnClick}>
                            <Col md={4}>
                                <FormGroup>
                                    <ControlLabel>Recipt Number</ControlLabel>
                                    <input
                                        type="number"
                                        className="form-control"
                                        value={reciptNumber}
                                        onChange={(e) => setReciptNumber(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Button
                                        className="btn-submit" fill type="submit">
                                        <i className={"fa fa-plus"} />Add</Button>
                                </FormGroup>
                                <FormGroup>
                                    {error && <label style={{ color: 'red', fontWeight: "bold" }} >درست  بل  نمبر  درج  کریں</label>}
                                </FormGroup>
                            </Col>
                        </Form>
                        <Col md={8}>
                            <Table responsive className="custom-table-wrapper">
                                <thead>

                                    <tr>
                                        <th>ٹوتل  بل</th>
                                        <th>نام</th>
                                        <th>بل  نمبر</th>
                                        <th>تاریح</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {
                                        priceArray.map((element, key) => (
                                            <tr key={key}>
                                                <th>{element.total}</th>
                                                <th>{element.name}</th>
                                                <th>{element.serialNumber}</th>
                                                <th>{dateFormate(element.date)}</th>
                                                <th onClick={()=>{
                                                    var newPriceArray=[...priceArray]
                                                    newPriceArray.splice(key, 1)
                                                    setPriceArray(newPriceArray)
                                                }} style={{color:'red',cursor:"pointer"}} >
                                                   <i className="fa fa-trash"></i> 
                                                </th>

                                            </tr>
                                        ))
                                    }
                                    <tr>
                                        <th style={{
                                            fontWeight:"bold",
                                            color:'red',
                                            fontSize:"20px"
                                        }} colSpan="2">{subTotal}</th>
                                        <th style={{
                                            fontWeight:"bold",
                                            color:'red',
                                            fontSize:"20px"
                                        }}colSpan="2">ٹوٹل</th>
                                    </tr>
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default AddingReciptModal