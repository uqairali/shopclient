import React, { useState } from 'react'
import { Grid, Modal, Form,FormGroup,ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { addClientName } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const AddClientDetails = ({ show, hide,fetchData }) => {
  const { handleSubmit, register, errors } = useForm();
  const [loading, setLoading] = useState(false)
  const [serverError, setServerError] = useState(null)
  const onSubmit = async (values) => {
    try {
      const data = {
        name: values.name,
        address: values.address,
        contactNumber:values.contactNumber
      }
      setLoading(true)
      setServerError(null)
      var res=await addClientName(data)
      setLoading(false)
      if(res.data){
        fetchData()
        hide()
      }

    } catch (err) {
      setLoading(false)
      setServerError(err.message)
     // onSowNotification('error',err.message)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add Details
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup
              validationState={errors.name && errors.name.message ? "error" : "success"}
            >
              <ControlLabel>Name</ControlLabel>
              <input
              
                name="name"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Customer Name"
                className="form-control"
                
              />
              {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.address && errors.address.message ? "error" : "success"}
            >
              <ControlLabel>Address</ControlLabel>
              <input
              
                name="address"
                ref={register({
                })}
                placeholder="Enter Address"
                className="form-control"
                
              />
              {(errors.address && errors.address.message) &&
               <small className="text-danger">{errors.address && errors.address.message}</small>}
            </FormGroup>
            <FormGroup
              validationState={errors.contactNumber && errors.contactNumber.message ? "error" : "success"}
            >
              <ControlLabel>Contact Number</ControlLabel>
              <input
              type="number"
                name="contactNumber"
                ref={register({
                })}
                placeholder="Enter Contact Number"
                className="form-control"
                
              />
              {(errors.contactNumber && errors.contactNumber.message) &&
               <small className="text-danger">{errors.contactNumber && errors.contactNumber.message}</small>}
            </FormGroup>

           
              {
                serverError&&
                <div className="center">
                  <small className="text-danger">{serverError}</small>
                </div>
              }

            <div className="center">
              <Button className="btn-submit" fill type="submit"><i className={loading?"fa fa-spin fa-spinner":"fa fa-check"} />Submit</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    
    </Grid>
  )
}

export default AddClientDetails