import React, { useState } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel,Row,Col } from 'react-bootstrap'
import { dateFormate2 } from 'utils/helper'
import Button from "components/CustomButton/CustomButton.jsx";
import { editItem } from 'api/api'
const EditItemModal = ({ onGetItems,selectedObj, show, hide }) => {
    const [itemName,setItemName]=useState(selectedObj.name)
const [particulars,setParticulars]=useState(selectedObj.particulars)
const [isValidInput,setIsValidInput]=useState(true)
    const onHandleEdit=async()=>{
        const data = {
            name: itemName,
            particulars:particulars.filter(fil=>fil!=='')
          }
          if(data.name===""||!data.particulars.length){
              setIsValidInput(false)
              return
          } 
          try{
         await editItem(selectedObj._id,data)
         onGetItems()
         hide()
          }catch(err){
              console.error(err)
          }
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Edit Item
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <FormGroup>
                  <ControlLabel>Item</ControlLabel>
                  <input
                    name="itemName"
                    value={itemName}
                    onChange={(e)=>setItemName(e.target.value)}
                    placeholder="Enter Item Name"
                    className="form-control"

                  />
                </FormGroup>
                <Row>
                <Col md={2}>
                <ControlLabel>Particular</ControlLabel>
                <input
                  name="product"
                  value={particulars[0]}
                  placeholder="Enter Item Name"
                  className="form-control"
                  onChange={e =>{
                    var newParticular=[...particulars]
                    newParticular[0]=e.target.value
                    setParticulars(newParticular)
                  }}
                />
              </Col>
              {
                particulars.map((part, key) => (
                  <Col md={2}>
                    <ControlLabel>Particular</ControlLabel>
                    <input
                      name="product"
                      value={particulars[key + 1]}
                      onChange={e=>{
                        var newParticular=[...particulars]
                        newParticular[key+1]=e.target.value
                        setParticulars(newParticular)
                      }}
                      placeholder="Enter Item Name"
                      className="form-control"

                    />
                  </Col>
                ))
              }
                </Row>
                <br/>
                <Button
                  disabled={itemName===""||!particulars.length}
                  onClick={onHandleEdit}
                  className="btn-submit" fill>
                      <i className={"fa fa-check"} />
                  Submit</Button>
                  {!isValidInput && <small className="text-danger">Please Enter Item Name And Particular</small>}
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default EditItemModal