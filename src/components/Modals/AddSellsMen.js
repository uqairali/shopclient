import React, { useState } from 'react'
import { Grid, Modal, Form,FormGroup,ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { newSellsMen } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const AddSellsMen = ({ show, hide,fetchData,onSowNotification }) => {
  const { handleSubmit, register, errors } = useForm();
  const onSubmit = async (values) => {
    try {
      const data = {
        name: values.name,
        item: values.distribution,
        contactNumber:values.contactNumber
      }
      var res=await newSellsMen(data)

      if(res.data){
        fetchData()
        hide()
        onSowNotification('success',"New Record Addedd successfully")
      }

    } catch (err) {
      onSowNotification('error',err.message)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add New Sell Men
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup
              validationState={errors.name && errors.name.message ? "error" : "success"}
            >
              <ControlLabel>Name</ControlLabel>
              <input
              
                name="name"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Customer Name"
                className="form-control"
                
              />
              {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.distribution && errors.distribution.message ? "error" : "success"}
            >
              <ControlLabel>Distribution</ControlLabel>
              <input
                name="distribution"
                ref={register({
                })}
                placeholder="Enter Distribution"
                className="form-control"
                
              />
              {(errors.distribution && errors.distribution.message) &&
               <small className="text-danger">{errors.distribution && errors.distribution.message}</small>}
            </FormGroup>
            <FormGroup
              validationState={errors.contactNumber && errors.contactNumber.message ? "error" : "success"}
            >
              <ControlLabel>Contact Number</ControlLabel>
              <input
              type="number"
                name="contactNumber"
                ref={register({
                })}
                placeholder="Enter Contact Number"
                className="form-control"
                
              />
              {(errors.contactNumber && errors.contactNumber.message) &&
               <small className="text-danger">{errors.contactNumber && errors.contactNumber.message}</small>}
            </FormGroup>

           
             

            <div className="center">
              <Button className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    
    </Grid>
  )
}

export default AddSellsMen