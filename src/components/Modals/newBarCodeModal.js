import React, { useState,useEffect } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Row } from 'react-bootstrap'
import { getBarCode, newBarCode,upddateBarCode } from 'api/api'
import Button from "components/CustomButton/CustomButton.jsx";
const AddClass = ({ show, hide, onSowNotification,readBarCode,setReadBarCode }) => {
  const [name, setName] = useState('')
  const [price, setPrice] = useState('')
  const [barcodeId,setBarcodeId]=useState("")
  useEffect(()=>{
  if(readBarCode!==""){
    onHandleExestense()
  }
  },[readBarCode])
  const onHandleExestense = async () => {
    try {
      setName('')
      setPrice('')
      var data = await getBarCode(readBarCode)
      if (data.data) {
        setName(data.data.name)
        setPrice(data.data.price)
        setBarcodeId(data.data._id)
      }
     
    } catch (err) {

    }
  }
  const onSubmit = async () => {
    var data = {
      barCode: readBarCode,
      name: name,
      price: price
    }
    try {
      if(barcodeId){
       await upddateBarCode(barcodeId,{name,price})
      }else{
      var doc = await newBarCode(data)
      onSowNotification('success', "نیا  بارکوڈ  درج  ہوگیا  ھیں")
      }
    } catch (err) {

      // onSowNotification('error',err)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add BarCode
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <label style={{ textAlign: "center" }}>{readBarCode}</label>
          </Row>
          <FormGroup>
            <ControlLabel>Item Name</ControlLabel>
            <input
              value={name}
              onChange={e => setName(e.target.value)}
              placeholder="Enter Class Name"
              className="form-control"

            />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Price</ControlLabel>
            <input
              value={price}
              onChange={e => setPrice(e.target.value)}
              placeholder="Enter Class Name"
              className="form-control"

            />
          </FormGroup>


          <div className="center">
            <Button onClick={onSubmit} className="btn-submit" fill><i className={"fa fa-check"} />Submit</Button>
          </div>
        </Modal.Body>
      </Modal>

    </Grid>
  )
}

export default AddClass