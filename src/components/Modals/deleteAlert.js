import React from 'react'
import SweetAlert from 'react-bootstrap-sweetalert';
const DeleteAlert = ({hide,onDelete,text,buttonText }) => {

    return (
        <SweetAlert
            warning
            style={{ display: 'block', marginTop: '-100px' }}
            title={text?text:"Are you sure?"}
            onConfirm={() => {
               hide(false)
                onDelete();
            }}
            onCancel={() =>
               hide(false)
            }
            confirmBtnBsStyle="info"
            cancelBtnBsStyle="danger"
            confirmBtnText={buttonText?buttonText:"Yes, delete it!"}
            cancelBtnText="Cancel"
            showCancel
        >
        </SweetAlert>
    )
}

export default DeleteAlert