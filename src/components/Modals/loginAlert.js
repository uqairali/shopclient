import {
    Grid,
    Col,
    FormGroup,
    ControlLabel,
    Row,
    Modal,
    Form
  } from "react-bootstrap";
  
  import Card from "components/Card/Card.jsx";
  
  import Button from "components/CustomButton/CustomButton.jsx";
  import { Redirect } from "react-router-dom";
  import React, { useState } from "react";
  
  import { login } from 'api/api'
  import useForm from "react-hook-form";
  import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";

const LoginAlert = ({  show, hide,onDelete }) => {
 
    const [serverError, setServerError] = useState();
    const [loading, setLoading] = useState(false);
    const { handleSubmit, register, errors } = useForm();
    const [OnShowPassword,setOnShowPassword]=useState(false)
    const onSubmit = async (values) => {
      try{
      setServerError(null);
     setLoading(true)
     let response = await login(values.email, values.password);
      setLoading(false)
      if(response.data){
        onDelete()
      }
    }
    
    catch(err){
      setLoading(false)
      setServerError(err.message);
    }
  
    }
  
 
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            LogIn To Confirm
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="login-alert">
        <section className="about">
        </section>
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Log In"
                ctAllIcons
                content={
                  <Form onSubmit={handleSubmit(onSubmit)}>

                    <small className="text-danger">{serverError || null}</small>
                    <FormGroup
                      validationState={errors.email && errors.email.message ? "error" : "success"}
                    >
                      <ControlLabel>Email address</ControlLabel>
                      <input
                        name="email"
                        ref={register({
                          required: 'Required',
                          pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                            message: "invalid email address"
                          }
                        })}
                        placeholder="Enter your email"
                        className="form-control"
                      />

                      {(errors.email && errors.email.message) && <small className="text-danger">{errors.email && errors.email.message}</small>}


                    </FormGroup>
                    <FormGroup
                      validationState={errors.password && errors.password.message ? "error" : "success"}
                    >
                      <ControlLabel>Password</ControlLabel>
                      <input
                        name="password"
                        type={OnShowPassword?"text":'password'}
                        ref={register({
                          required: 'Required',

                        })}
                        placeholder="Enter your password"
                        className="form-control"
                      />
                      {(errors.password && errors.password.message) && <small className="text-danger">{errors.password && errors.password.message}</small>}

                    </FormGroup>
                    <FormGroup style={{color:'white'}} >
                    <Checkbox
                            checked={OnShowPassword}
                            number="1"
                            label="Show Password"
                            onClick={()=>setOnShowPassword(!OnShowPassword)}
                          />
                    </FormGroup>
                    <div className="center">
                      <Button disabled={loading} className="btn-submit" fill type="submit"><i className={loading?"fa fa-spin fa-spinner":"fa fa-lock"}/>SIGN IN</Button>
                    </div>
                  </Form>


                } />

            </Col>

          </Row>
        </Grid>
      </div >
        </Modal.Body>
      </Modal>

    </Grid>
  )
}

export default LoginAlert