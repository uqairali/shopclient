import React, { useState, useEffect, Fragment } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel, Row, Col, Table, Badge } from 'react-bootstrap'
import useForm from "react-hook-form";
import { addItemType } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
import Radio from 'components/CustomRadio/CustomRadio';
import Select from "react-select";
const AddItemType = ({ show, hide, selectedObj, fetchData, onSowNotification }) => {
  const { handleSubmit, register, errors, reset } = useForm();
  const [addItemTypeLoading, setAddItemTypeLoading] = useState(false)
  const [radio, setRadio] = useState(selectedObj.particulars.length ? selectedObj.particulars[0] : '')
  const [priceArray, setPriceArray] = useState([])
  const [selectedParticular, setSelectedParticular] = useState([])
  const mapParticulars = selectedObj.particulars.map(part => {
    return { label: part, value: part }
  })
  const onSubmit = async (value) => {
    const data = {
      name: value.name,
      price: priceArray,
      count:{particular:selectedParticular.value,totalCount:value.count ? value.count : 0},
      itemId: selectedObj._id
    }
    try {
      setAddItemTypeLoading(true)
      await addItemType(data)
      reset({
        name: '',
        price: '',
        count: ''
      })
      setPriceArray([])
      setAddItemTypeLoading(false)
    } catch (err) {
      setAddItemTypeLoading(false)
    }
  }
  var filterInputPrice = priceArray.filter(val => val.particular === radio)
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Add Item Type
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup
              validationState={errors.item && errors.item.message ? "error" : "success"}
            >
              <ControlLabel>Item Name</ControlLabel>
              <input
                readOnly
                value={selectedObj.name}
                name="item"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Item Name"
                className="form-control"

              />
              {(errors.item && errors.item.message) && <small className="text-danger">{errors.item && errors.item.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.name && errors.name.message ? "error" : "success"}
            >
              <ControlLabel>Type Name</ControlLabel>
              <input

                name="name"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Type Name"
                className="form-control"

              />
              {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
            </FormGroup>
            <Row>
              {
                selectedObj.particulars.map((patcls, key) => {
                  var filter = priceArray.filter(val => val.particular === patcls)
                  return <Col style={{ color: !filter.length && 'red' }} key={key} md={2}>
                    <Radio
                      number={key}
                      option={patcls}
                      name="radio"
                      onChange={e => setRadio(e.target.value)}
                      checked={radio === patcls}
                      label={patcls}
                    />

                  </Col>
                })
              }

            </Row>
       <Row>
         <Col md={6}>
            <FormGroup>
              <ControlLabel>Price ({radio})</ControlLabel>
              <input
                type="number"
                name="price"
                value={filterInputPrice.length ? filterInputPrice[0].price : ''}
                onChange={e => {
                  var newSetPriceArray = [...priceArray]
                  if (newSetPriceArray.length && filterInputPrice.length) {
                    newSetPriceArray.map((val, k) => {
                      if (val.particular === radio){
                        newSetPriceArray[k].price =e.target.value
                        newSetPriceArray[k].particular= radio 
                      }
                    })
                  } else {
                    newSetPriceArray.push({ price: e.target.value, particular: radio })
                  }
                  setPriceArray(newSetPriceArray)
                }}
                readOnly={!radio}
                placeholder="Enter Price"
                className="form-control"

              />
            </FormGroup>
          </Col>
          <Col md={6}>
          <FormGroup>
              <ControlLabel>Contain</ControlLabel>
              <input
                type="number"
                name="Contain"
                value={filterInputPrice.length ? filterInputPrice[0].contain : ''}
                onChange={e => {
                  var newSetPriceArray = [...priceArray]
                  if (newSetPriceArray.length && filterInputPrice.length) {
                    newSetPriceArray.map((val, k) => {
                      if (val.particular === radio)
                        newSetPriceArray[k].contain= e.target.value
                    })
                  } else {
                    newSetPriceArray.push({ price: 0, particular: radio,contain:e.target.value })
                  }
                  setPriceArray(newSetPriceArray)
                }}
                readOnly={!radio}
                placeholder="Enter Price"
                className="form-control"

              />
            </FormGroup>
          </Col>
          </Row>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <ControlLabel>Select Particular</ControlLabel>
                  <Select
                    clearable={false}
                    placeholder={"Single Select"}
                    value={selectedParticular}
                    options={mapParticulars}
                    onChange={data => {
                      if (data) {
                        setSelectedParticular(data)
                      }
                    }}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup
                  validationState={errors.count && errors.count.message ? "error" : "success"}
                >
                  <ControlLabel>Total Count</ControlLabel>
                  <input
                    type="number"
                    name="count"
                    ref={register({
                      //required: 'Required',
                    })}
                    placeholder="Enter Total Count"
                    className="form-control"

                  />
                  {(errors.count && errors.count.message) && <small className="text-danger">{errors.count && errors.count.message}</small>}
                </FormGroup>
              </Col>
            </Row>
            <Button
              disabled={addItemTypeLoading}
              className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>

          </Form>

        </Modal.Body>
      </Modal>

    </Grid>
  )
}

export default AddItemType