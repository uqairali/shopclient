import React, { useState } from 'react'
import { Grid, Modal, Form,FormGroup,ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { updateSellsMen } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const EditSellsMen = ({ show, hide,fetchData,selectedObj,onSowNotification }) => {
  const { handleSubmit, register, errors } = useForm();
  const onSubmit = async (values) => {
    try {
      const data = {
        name: values.name,
        address: values.distribution,
        contactNumber:values.contactNumber,
        item:values.distribution
      }
      var res=await updateSellsMen(selectedObj._id,data)
      if(res.data){
        fetchData()
        hide()
        onSowNotification('success',"New Record Addedd successfully")
      }

    } catch (err) {
      onSowNotification('error',err.message)
    }

  }
  return (
    <Grid fluid>
      <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
        <Modal.Header closeButton>
          <Modal.Title>
            Edit Details
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
            <FormGroup
              validationState={errors.name && errors.name.message ? "error" : "success"}
            >
              <ControlLabel>Name</ControlLabel>
              <input
              defaultValue={selectedObj.name}
                name="name"
                ref={register({
                  required: 'Required',
                })}
                placeholder="Enter Customer Name"
                className="form-control"
                
              />
              {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
            </FormGroup>

            <FormGroup
              validationState={errors.distribution && errors.distribution.message ? "error" : "success"}
            >
              <ControlLabel>Distribution</ControlLabel>
              <input
              defaultValue={selectedObj.item&&selectedObj.item}
                name="distribution"
                ref={register({
                })}
                placeholder="Enter distribution"
                className="form-control"
                
              />
              {(errors.distribution && errors.distribution.message) &&
               <small className="text-danger">{errors.distribution && errors.distribution.message}</small>}
            </FormGroup>
            <FormGroup
              validationState={errors.contactNumber && errors.contactNumber.message ? "error" : "success"}
            >
              <ControlLabel>Contact Number</ControlLabel>
              <input
              defaultValue={selectedObj.contactNumber&&selectedObj.contactNumber}
              type="number"
                name="contactNumber"
                ref={register({
                })}
                placeholder="Enter Contact Number"
                className="form-control"
                
              />
              {(errors.contactNumber && errors.contactNumber.message) &&
               <small className="text-danger">{errors.contactNumber && errors.contactNumber.message}</small>}
            </FormGroup>

           
            

            <div className="center">
              <Button className="btn-submit" fill type="submit"><i className={"fa fa-check"} />Submit</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    
    </Grid>
  )
}

export default EditSellsMen