import React, { useState, useEffect } from 'react'
const About=()=>{
    var [count, setCount] = useState(0)
    var [count2, setCount2] = useState(0)
   
    useEffect(() => {
        const interval = setInterval(() => {
         setCount(count+=1)
         if(count>365)
         clearInterval(interval)
        },2);

        const interval2 = setInterval(() => {
            setCount2(count2+=1)
            if(count2>7)
            clearInterval(interval2)
           },200);
      }, []);
    return(
        <div className="container">
        <div className="box">
            <span>{count2<=7?count2:"7"}</span>
            <h6>Continents<br />On Earth</h6>
        </div>
        <div className="box">
            <span>{count<=196?count:"196"}</span>
            <h6>Unique<br />Countries</h6>
        </div>
        <div className="box">
            <span>{count<=365?count:"365"}</span>
            <h6>days<br />per year</h6>
        </div>
        <div className="box">
            <span>{count2<=4?count2:"4"}</span>
            <h6>Average<br />Trips Per Year</h6>
        </div>
        <div className="box">
            <span>{count2<=6?count2:"6"}</span>
            <h6>Average<br />Days Per Vacation</h6>
        </div>
    </div>
    )
}

export default About