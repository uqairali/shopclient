import React from 'react';
const ImageIcon = ({ x,current, index, opaque, size, pan }) => {

    const style = {
        opacity: opaque ? .5 : 1,
        height: size ? size : 50,
        width: size ? size : 50,
        cursor: "pointer",
        backgroundColor: "#1022ef"
    }
    return <div className="circle-space">
       {x._id!==current._id&& <span>
            <i style={{ color: "#1022ef", fontWeight: 'bold',opacity: opaque ? .5 : 1 }} className="fa fa-chevron-circle-up" />
        </span>}
        <div onClick={pan} className="nav-circle" style={style} >
            {x.name}
        </div>
       {x._id===current._id&& <span>
            <i style={{ color: "#1022ef", fontWeight: 'bold',opacity: opaque ? .5 : 1 }} className="fa fa-chevron-circle-down" />
        </span>}

    </div>


};

export default ImageIcon;

