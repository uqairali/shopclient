import React from 'react';
const ImageIcon = ({  opaque, size }) => {

    const style = {
        opacity: opaque ? .5 : 1,
        height: size ? size : 50,
        width: size ? size : 50,
        cursor: "pointer",
        backgroundColor: "rgb(26, 28, 28)"
    }
    return <div style={{marginTop:"20px"}} className="circle-space">
        <div className="nav-circle" style={style} >
          Add Class
        </div>
        </div>
};

export default ImageIcon;

