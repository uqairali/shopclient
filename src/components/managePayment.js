import React from 'react'
import resolve from 'api/resolve'
import { Row,Col,FormGroup,ControlLabel } from 'react-bootstrap'
const ManagePayment=({payment,setPayment,remining,setRemining})=>{
    return(
        <Row className="items-show manage-payment">
           
            <Col md={4}>
                <FormGroup>
                    <ControlLabel>سابقہ</ControlLabel>
                    <input
                        type={"text"}
                        className="form-control"
                        value={remining}
                        onChange={e => e.target.value!==" "&&setRemining(e.target.value)}
                    />
                </FormGroup>
            </Col>
            <Col md={4}>
                <FormGroup>
                    <ControlLabel>وصول</ControlLabel>
                    <input
                        type={"number"}
                        className="form-control"
                        value={payment}
                        onChange={e => e.target.value!==" "&&setPayment(e.target.value)}
                    />
                </FormGroup>
            </Col>
        </Row>
    )
}

export default ManagePayment