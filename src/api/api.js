import axios from 'axios';
import resolve from './resolve';

  let apiBase = process.env.REACT_APP_API_BASEURL;
let apiBaseUrl=apiBase+"/api"
const testAuth = async () => {
    return await resolve(axios.get(`${apiBaseUrl}/profile`).then(res => res.data));
}
const login = async (email, password) => {
    return await resolve(axios.post(`${apiBaseUrl}/auth/login`, {
        email,
        password
    }).then(res => res.data));
}

const getItemsTypes= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/types`).then(res => res.data));
}
const deleteItemType= async (typeId,productId) => {
    return await resolve(axios.delete(`${apiBaseUrl}/types/${typeId}/${productId}`).then(res => res.data));
}
const getItems= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/item`).then(res => res.data));
}
const addItem= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/item`,data).then(res => res.data));
}
const addItemType= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/types`,data).then(res => res.data));
}
const editItemType= async (data,id) => {
    return await resolve(axios.put(`${apiBaseUrl}/types/${id}`,data).then(res => res.data));
}

const updateItemCount= async (data) => {
    return await resolve(axios.put(`${apiBaseUrl}/typeTotalCount`,data).then(res => res.data));
}
const postRecipt= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/recipt`,data).then(res => res.data));
}
const updatRecipt= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/recipt/${id}`,data).then(res => res.data));
}
const getAllRecipt= async (date) => {
    return await resolve(axios.get(`${apiBaseUrl}/recipt/${date}`).then(res => res.data));
}
const deleteRecipt= async (id) => {
    return await resolve(axios.delete(`${apiBaseUrl}/recipt/${id}`).then(res => res.data));
}
const getSerialNumber= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/getSerialNumber`).then(res => res.data));
}
const updateSerialNumber= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/incrementSN`).then(res => res.data));
}

const editItem= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/item/${id}`,data).then(res => res.data));
}
const addClientName= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/clientName`,data).then(res => res.data));
}
const AllClientName= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/allClientsName`).then(res => res.data));
}
const deleteItem= async (id) => {
    return await resolve(axios.delete(`${apiBaseUrl}/item/${id}`).then(res => res.data));
}
const deleteClientName= async (id) => {
    return await resolve(axios.delete(`${apiBaseUrl}/clientName/${id}`).then(res => res.data));
}
const updateClientName= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/clientName/${id}`,data).then(res => res.data));
}
const getReciptbyId= async (id) => {
    return await resolve(axios.get(`${apiBaseUrl}/reciptById/${id}`).then(res => res.data));
}
const updateClientDetails= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/clientName/${id}`,data).then(res => res.data));
}
const allReciptByName= async (name) => {
    return await resolve(axios.get(`${apiBaseUrl}/reciptsbyname/${name}`).then(res => res.data));
}
const ReciptBySerialNumber= async (serialNumber) => {
    return await resolve(axios.get(`${apiBaseUrl}/reciptbyserialnumber/${serialNumber}`).then(res => res.data));
}
const addNewShortKey= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/newShortKey`,data).then(res => res.data));
}
const updateShortKey= async (shortKey,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/updateShortKey/${shortKey}`,data).then(res => res.data));
}
const shortKeyByKey= async (key) => {
    return await resolve(axios.get(`${apiBaseUrl}/getByKey/${key}`).then(res => res.data));
}
const getAllShortKey= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/allShortkeys`).then(res => res.data));
}
const newSellsMen= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/newSellsMen`,data).then(res => res.data));
}
const getAllSellsMen= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/allSellsMen`).then(res => res.data));
}
const updateSellsMen= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/updateSellsMen/${id}`,data).then(res => res.data));
}
const deleteSellsMen= async (id) => {
    return await resolve(axios.delete(`${apiBaseUrl}/deleteSellsMen/${id}`).then(res => res.data));
}
const newSellsMenDetails= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/newSellsMenDetails`,data).then(res => res.data));
}
const sillsMenDetails= async (date) => {
    return await resolve(axios.get(`${apiBaseUrl}/sellsMenDetails/${date}`).then(res => res.data));
}
const deleteSellsMenDetails= async (id,payment,isPayed,sellsMenId) => {
    return await resolve(axios.delete(`${apiBaseUrl}/deleteSellsMendetails/${id}/${payment}/${isPayed}/${sellsMenId}`).then(res => res.data));
}
const sillsMenDetailsById= async (id) => {
    return await resolve(axios.get(`${apiBaseUrl}/getById/${id}`).then(res => res.data));
}
const getAllAmmount= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/getAllAmmount`).then(res => res.data));
}
const updateSellsMenDetails= async (id,data,preVal,sellsMenId) => {
    return await resolve(axios.put(`${apiBaseUrl}/updateSellsMenDetails/${id}/${preVal}/${sellsMenId}`,data).then(res => res.data));
}
const getBarCode= async (barCode) => {
    return await resolve(axios.get(`${apiBaseUrl}/getBarCode/${barCode}`).then(res => res.data));
}
const newBarCode= async (data) => {
    return await resolve(axios.post(`${apiBaseUrl}/newBarCode`,data).then(res => res.data));
}
const getPendingRecipts= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/getPendingRecipts`).then(res => res.data));
}

const getAllRecipts= async () => {
    return await resolve(axios.get(`${apiBaseUrl}/reciptGetAll`).then(res => res.data));
}
const upddateBarCode= async (id,data) => {
    return await resolve(axios.put(`${apiBaseUrl}/updateBarCode/${id}`,data).then(res => res.data));
}

export {
    upddateBarCode,
    apiBase,
    testAuth,
    login,
    getItemsTypes,
    deleteItemType,
    getItems,
    addItem,
    addItemType,
    editItemType,
    updateItemCount,
    postRecipt,
    getAllRecipt,
    deleteRecipt,
    getSerialNumber,
    updateSerialNumber,
    editItem,
    addClientName,
    AllClientName,
    deleteItem,
    deleteClientName,
    updateClientName,
    getReciptbyId,
    updateClientDetails,
    updatRecipt,
    allReciptByName,
    ReciptBySerialNumber,
    addNewShortKey,
    updateShortKey,
    shortKeyByKey,
    getAllShortKey,
    newSellsMen,
    getAllSellsMen,
    updateSellsMen,
    deleteSellsMen,
    newSellsMenDetails,
    sillsMenDetails,
    deleteSellsMenDetails,
    sillsMenDetailsById,
    getAllAmmount,
    updateSellsMenDetails,
    getBarCode,
    newBarCode,
    getPendingRecipts,
    getAllRecipts
}
