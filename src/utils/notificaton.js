import React from 'react'
import { connect } from 'react-redux'

const Notification=({type,errorMsg,onShowNotification})=>{
    alert()
    onShowNotification(type,errorMsg)
    return<></>
}

const mapDispatchToProps = dispatch => {
    return {//errorType should be be error,info,warning and success by default it is success
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}

export default connect(null, mapDispatchToProps)(Notification)