import React, { Fragment } from 'react'
import { Helmet } from "react-helmet";
import { AppName } from 'utils/const'
import routes from "routes.js";
const SetTitle = (props) => {
    const getActiveRoute = routes => {
        let activeRoute = "Default Brand Text";
        for (let i = 0; i < routes.length; i++) {
            if (routes[i].collapse) {
                let collapseActiveRoute = getActiveRoute(routes[i].views);
                if (collapseActiveRoute !== activeRoute) {
                    return collapseActiveRoute;
                }
            } else {
                if (
                    window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1
                ) {
                    return routes[i].name;
                }
            }
        }
        return activeRoute;
    }

    return (
        <Fragment>
            <Helmet>
                <title>{AppName + " | " + getActiveRoute(routes)}</title>
            </Helmet>
        </Fragment>
    )
}

export default SetTitle