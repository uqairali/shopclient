
export const initialState = {
    //set notification
    notificationMessage: false,
    notificationType: 'error',
    loading:false,

}



const reducer = (state = initialState, action) => {
    switch (action.type) {
     
        //catch error
        case "SET_NOTIFICATION":
            return {
                ...state,
                notificationMessage: action.errorMessage,
                notificationType: action.errorType,

            }


        case "CLEAR_NOTIFICATION":
            return {
                ...state,
                notificationMessage: false,

            }
            case "SET_LOADING":
                return {
                    ...state,
                    loading:action.setLoading,
    
                }



        default:
            return state;
    }
}

export default reducer;